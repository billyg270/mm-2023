package uk.co.cablepost.mm2023.mixin;

import net.minecraft.command.argument.EntityAnchorArgumentType;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.scoreboard.AbstractTeam;
import net.minecraft.scoreboard.Team;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.Vec3d;
import org.objectweb.asm.Opcodes;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.Redirect;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(PlayerEntity.class)
public class PlayerEntityMixin {

    @Inject(method = "getAttackCooldownProgressPerTick()F", at = @At("HEAD"), cancellable = true)
    public void inject(CallbackInfoReturnable<Float> cir) {
        cir.setReturnValue(0.0f);
    }

    @Inject(method = "getAttackCooldownProgress(F)F", at = @At("HEAD"), cancellable = true)
    public void inject(float baseTime, CallbackInfoReturnable<Float> cir) {
        cir.setReturnValue(1.0f);
    }

    @Redirect(method = "tick()V", at = @At(value = "FIELD", target = "Lnet/minecraft/entity/player/PlayerEntity;noClip:Z", opcode = Opcodes.PUTFIELD, ordinal = 0))
    public void inject(PlayerEntity instance, boolean value){
        AbstractTeam team = instance.getScoreboardTeam();
        if(team != null) {
            instance.noClip = false;
        }
        else {
            instance.noClip = instance.isSpectator();
        }
    }
}
