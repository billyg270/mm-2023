package uk.co.cablepost.mm2023.mixin;

import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.entity.damage.DamageType;
import net.minecraft.registry.tag.DamageTypeTags;
import net.minecraft.registry.tag.TagKey;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(DamageSource.class)
public class DamageSourceMixin {
//    @Inject(method = "isIn(Lnet/minecraft/registry/tag/TagKey;)Z", at = @At("HEAD"), cancellable = true)
//    void inject(TagKey<DamageType> tag, CallbackInfoReturnable<Boolean> cir) {
//        if(tag == DamageTypeTags.BYPASSES_COOLDOWN){
//            cir.setReturnValue(true);
//        }
//    }
}
