package uk.co.cablepost.mm2023.mixin;

import net.minecraft.block.Block;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(Block.class)
public class BlockMixin {
    @Inject(
            method = "getBlastResistance()F",
            at = @At("HEAD"),
            cancellable = true
    )
    private void inject(CallbackInfoReturnable<Float> cir) {
        Block block = ((Block)(Object)this);
        if(block.getTranslationKey().contains("_wool")){
            cir.setReturnValue(3600000.0f);
            cir.cancel();
        }
    }
}
