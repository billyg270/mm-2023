package uk.co.cablepost.mm2023.mixin;

import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.gui.screen.multiplayer.MultiplayerScreen;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import uk.co.cablepost.mm2023.client.screen.MmTitleScreen;

import static uk.co.cablepost.mm2023.client.Mm2023Client.OVERRIDE_TITLE_SCREEN;

@Environment(value = EnvType.CLIENT)
@Mixin(MultiplayerScreen.class)
public class MultiplayerScreenMixin {
    @Inject(method = "tick()V", at = @At("HEAD"), cancellable = true)
    public void inject(CallbackInfo ci) {
        if(OVERRIDE_TITLE_SCREEN){
            MinecraftClient.getInstance().setScreen(new MmTitleScreen());
            ci.cancel();
        }
    }
}
