package uk.co.cablepost.mm2023.mixin;

import net.minecraft.entity.player.HungerManager;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.world.GameRules;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import java.util.Objects;

@Mixin(HungerManager.class)
public class HungerManagerMixin {
    @Inject(method = "getFoodLevel()I", at = @At("HEAD"), cancellable = true)
    public void Inject(CallbackInfoReturnable<Integer> cir){
        cir.setReturnValue(20);
    }

    @Inject(method = "getPrevFoodLevel()I", at = @At("HEAD"), cancellable = true)
    public void Inject2(CallbackInfoReturnable<Integer> cir){
        cir.setReturnValue(20);
    }

    @Inject(method = "isNotFull()Z", at = @At("HEAD"), cancellable = true)
    public void Inject3(CallbackInfoReturnable<Boolean> cir){
        cir.setReturnValue(false);
    }

    @Inject(method = "getExhaustion()F", at = @At("HEAD"), cancellable = true)
    public void Inject4(CallbackInfoReturnable<Float> cir){
        cir.setReturnValue(0f);
    }

    @Inject(method = "getSaturationLevel()F", at = @At("HEAD"), cancellable = true)
    public void Inject5(CallbackInfoReturnable<Float> cir){
        cir.setReturnValue(5.0f);
    }

    @Inject(method = "update(Lnet/minecraft/entity/player/PlayerEntity;)V", at = @At("HEAD"), cancellable = true)
    public void Inject(PlayerEntity player, CallbackInfo ci){
        if(Objects.requireNonNull(player.getWorld().getServer()).getTicks() % 100 == 0 && player.getWorld().getGameRules().getBoolean(GameRules.NATURAL_REGENERATION)){
            player.heal(0.5f);
        }

        ci.cancel();
    }
}
