package uk.co.cablepost.mm2023.mixin;

import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.MinecraftClient;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import uk.co.cablepost.mm2023.mixinFunc.MinecraftClientMixinFunc;

@Environment(value = EnvType.CLIENT)
@Mixin(MinecraftClient.class)
public class MinecraftClientMixin {
    @Inject(method = "scheduleStop()V", at = @At("HEAD"), cancellable = true)
    void injectScheduleStop(CallbackInfo ci) {
        if(MinecraftClientMixinFunc.injectScheduleStop()){
            ci.cancel();
        }
    }
}
