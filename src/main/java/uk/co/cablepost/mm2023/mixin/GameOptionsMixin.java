package uk.co.cablepost.mm2023.mixin;

import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.option.GameOptions;
import net.minecraft.client.option.SimpleOption;
import net.minecraft.text.Text;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Unique;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import uk.co.cablepost.mm2023.client.Mm2023Client;

@Environment(value = EnvType.CLIENT)
@Mixin(GameOptions.class)
public class GameOptionsMixin {

    @Unique
    private final SimpleOption<Double> mouseSensitivity = new SimpleOption<>("options.noSensitivity", SimpleOption.emptyTooltip(), (optionText, value) -> GameOptions.getGenericValueText(optionText, Text.translatable("options.sensitivity.min")), SimpleOption.DoubleSliderCallbacks.INSTANCE, -0.33333333d, value -> {});

    @Inject(method = "getMouseSensitivity()Lnet/minecraft/client/option/SimpleOption;", at = @At("HEAD"), cancellable = true)
    private void injected(CallbackInfoReturnable<SimpleOption<Double>> cir) {
        if(!Mm2023Client.CAN_LOOK) {
            cir.setReturnValue(mouseSensitivity);
        }
    }
}
