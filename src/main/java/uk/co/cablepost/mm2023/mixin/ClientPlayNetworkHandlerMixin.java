package uk.co.cablepost.mm2023.mixin;

import net.minecraft.client.MinecraftClient;
import net.minecraft.client.network.ClientPlayNetworkHandler;
import net.minecraft.entity.Entity;
import net.minecraft.network.packet.s2c.play.DamageTiltS2CPacket;
import net.minecraft.network.packet.s2c.play.GameStateChangeS2CPacket;
import net.minecraft.world.GameMode;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import uk.co.cablepost.mm2023.client.Mm2023Client;

@Mixin(ClientPlayNetworkHandler.class)
public class ClientPlayNetworkHandlerMixin {
    @Inject(method = "onGameStateChange(Lnet/minecraft/network/packet/s2c/play/GameStateChangeS2CPacket;)V", at = @At("RETURN"))
    public void inject(GameStateChangeS2CPacket packet, CallbackInfo ci){
        GameStateChangeS2CPacket.Reason reason = packet.getReason();
        if (reason == GameStateChangeS2CPacket.GAME_MODE_CHANGED) {
            MinecraftClient client = MinecraftClient.getInstance();
            assert client.interactionManager != null;
            assert client.player != null;

            if(client.interactionManager.getCurrentGameMode() == GameMode.SPECTATOR) {
                client.player.setPitch(90f);
                client.player.setVelocity(0, 2, 0);
            }
        }
    }
}
