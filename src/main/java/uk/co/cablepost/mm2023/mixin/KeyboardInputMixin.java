package uk.co.cablepost.mm2023.mixin;

import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.input.KeyboardInput;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import uk.co.cablepost.mm2023.client.Mm2023Client;

@Environment(value = EnvType.CLIENT)
@Mixin(KeyboardInput.class)
public class KeyboardInputMixin {
    @Inject(method = "tick(ZF)V", at = @At("HEAD"), cancellable = true)
    private void injected(boolean slowDown, float slowDownFactor, CallbackInfo ci) {
        KeyboardInput thisObject = (KeyboardInput)(Object)this;

        if(!Mm2023Client.CAN_MOVE){
            thisObject.pressingForward = false;
            thisObject.pressingBack = false;
            thisObject.pressingLeft = false;
            thisObject.pressingRight = false;
            thisObject.movementForward = 0f;
            thisObject.movementSideways = 0f;
            thisObject.jumping = false;
            thisObject.sneaking = false;

            ci.cancel();
        }

        if(Mm2023Client.B_INPUT){
            thisObject.pressingForward = Mm2023Client.B_INPUT_FORWARD > 0;
            thisObject.pressingBack = Mm2023Client.B_INPUT_FORWARD < 0;
            thisObject.pressingLeft = Mm2023Client.B_INPUT_SIDEWAYS < 0;
            thisObject.pressingRight = Mm2023Client.B_INPUT_SIDEWAYS > 0;
            thisObject.movementForward = Mm2023Client.B_INPUT_FORWARD;
            thisObject.movementSideways = Mm2023Client.B_INPUT_SIDEWAYS;
            thisObject.jumping = Mm2023Client.B_INPUT_JUMP;
            thisObject.sneaking = Mm2023Client.B_INPUT_SNEAK;
            if (slowDown) {
                thisObject.movementSideways *= slowDownFactor;
                thisObject.movementForward *= slowDownFactor;
            }

            ci.cancel();
        }
    }
}
