package uk.co.cablepost.mm2023.mixin;

import net.minecraft.client.Keyboard;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import uk.co.cablepost.mm2023.client.Mm2023Client;

@Mixin(Keyboard.class)
public class KeyboardMixin {
    @Inject(method = "onKey(JIIII)V", at = @At("HEAD"))
    void inject(long window, int key, int scancode, int action, int modifiers, CallbackInfo ci) {
        if (action == 1 || action == 2) {
            Mm2023Client.keyboardInputs.keyPressed(key);
        } else if (action == 0) {
            Mm2023Client.keyboardInputs.keyReleased(key);
        }
    }
}
