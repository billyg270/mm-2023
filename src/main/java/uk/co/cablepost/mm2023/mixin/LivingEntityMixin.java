package uk.co.cablepost.mm2023.mixin;

import net.fabricmc.fabric.api.networking.v1.PacketByteBufs;
import net.fabricmc.fabric.api.networking.v1.ServerPlayNetworking;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.server.network.ServerPlayerEntity;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Unique;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import uk.co.cablepost.mm2023.Mm2023;

import java.util.Objects;

@Mixin(LivingEntity.class)
public class LivingEntityMixin {

    @Unique
    private static final int DAMAGE_COOLDOWN = 15;

    @Inject(method = "onDamaged(Lnet/minecraft/entity/damage/DamageSource;)V", at = @At("TAIL"))
    void inject(DamageSource damageSource, CallbackInfo ci) {
        LivingEntity livingEntity = ((LivingEntity)(Object)this);
        livingEntity.timeUntilRegen = DAMAGE_COOLDOWN;
        livingEntity.hurtTime = DAMAGE_COOLDOWN - 2;
        livingEntity.maxHurtTime = DAMAGE_COOLDOWN - 2;
    }

    @Inject(method = "damage(Lnet/minecraft/entity/damage/DamageSource;F)Z", at = @At("RETURN"))
    void injectR(DamageSource source, float amount, CallbackInfoReturnable<Boolean> cir) {
        if(cir.getReturnValue()){
            LivingEntity thisObject = (LivingEntity)(Object)this;
            if(source.getAttacker() != null && source.getAttacker().isPlayer() && !thisObject.getWorld().isClient()){
                ServerPlayerEntity attackerPlayer = Objects.requireNonNull(thisObject.getServer()).getPlayerManager().getPlayer(source.getAttacker().getUuid());

                PacketByteBuf buf = PacketByteBufs.create();
                buf.writeInt(thisObject.getId());
                assert attackerPlayer != null;
                ServerPlayNetworking.send(attackerPlayer, Mm2023.SET_LAST_ATTACK_PACKET_ID, buf);
            }

            LivingEntity livingEntity = ((LivingEntity) (Object) this);
            livingEntity.timeUntilRegen = DAMAGE_COOLDOWN;
            livingEntity.hurtTime = DAMAGE_COOLDOWN - 2;
            livingEntity.maxHurtTime = DAMAGE_COOLDOWN - 2;
        }
    }
}
