package uk.co.cablepost.mm2023.mixin;

import net.minecraft.server.dedicated.MinecraftDedicatedServer;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import uk.co.cablepost.mm2023.Mm2023;
import uk.co.cablepost.mm2023.server.ConnectingPlayerModel;
import uk.co.cablepost.mm2023.server.MmStaticServerData;
import uk.co.cablepost.mm2023.server.OrchestratorApiClientForServer;
import uk.co.cablepost.mm2023.server.minigame.OneVsOnePvpMinigame;

@Mixin(MinecraftDedicatedServer.class)
public class MinecraftDedicatedServerMixin {
    @Inject(method = "setupServer()Z", at = @At("RETURN"))
    void inject(CallbackInfoReturnable<Boolean> cir) {
        if(!cir.getReturnValue()){
            return;// Server failed to start
        }

        String apiToken = System.getenv("MM_API_TOKEN");
        if(apiToken == null || apiToken.trim().isEmpty()) {
            Mm2023.LOGGER.info("No MM_API_TOKEN provided, will not connect to API");

            if(System.getenv("MM_DEBUG_MINIGAME") != null && System.getenv("MM_DEBUG_MINIGAME").trim().equalsIgnoreCase("true")) {
                Mm2023.LOGGER.info("Using MM_DEBUG_MINIGAME");
                MmStaticServerData.SERVER_NAME = "TEST";
                MmStaticServerData.MAP_NAME = "PvpBedrockBoxes";
                MmStaticServerData.GAMEMODE = "1v1PVP";
                MmStaticServerData.MAP_X = 458;
                MmStaticServerData.MAP_Y = -57;
                MmStaticServerData.MAP_Z = 248;
                MmStaticServerData.TEAM_COUNT = 2;
                MmStaticServerData.MIN_TEAM_PLAYERS = 1;
                MmStaticServerData.MAX_TEAM_PLAYERS = 1;
                MmStaticServerData.MINIGAME_INSTANCE = new OneVsOnePvpMinigame();
            }

            return;
        }
        MmStaticServerData.SERVER_NAME = System.getenv("MM_SERVER_NAME");
        MmStaticServerData.ORCHESTRATOR_API_CLIENT_FOR_SERVER = new OrchestratorApiClientForServer(apiToken.trim());
    }
}
