package uk.co.cablepost.mm2023.mixin;

import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.gui.DrawContext;
import net.minecraft.client.gui.hud.InGameHud;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import uk.co.cablepost.mm2023.mixinFunc.InGameHudMixinFunc;

@Environment(value = EnvType.CLIENT)
@Mixin(InGameHud.class)
public class InGameHudMixin {
    @Inject(method = "renderStatusEffectOverlay(Lnet/minecraft/client/gui/DrawContext;)V", at = @At("HEAD"), cancellable = true)
    void injectRenderStatusEffectOverlay(DrawContext context, CallbackInfo ci) {
        InGameHud thisObject = (InGameHud)(Object)this;
        InGameHudMixinFunc.injectRenderStatusEffectOverlay(context, ci, thisObject);
        ci.cancel();
    }

    @Inject(method = "renderHotbar(FLnet/minecraft/client/gui/DrawContext;)V", at = @At("HEAD"), cancellable = true)
    void injectRenderHotbar(float tickDelta, DrawContext context, CallbackInfo ci) {
        InGameHudMixinFunc.injectRenderHotbar(tickDelta, context, ci);
        ci.cancel();
    }

    @Inject(method = "renderStatusBars(Lnet/minecraft/client/gui/DrawContext;)V", at = @At("HEAD"), cancellable = true)
    void injectRenderStatusBars(DrawContext context, CallbackInfo ci){
        InGameHudMixinFunc.injectRenderStatusBars(context, ci);
        ci.cancel();
    }

    @Inject(method = "renderExperienceBar(Lnet/minecraft/client/gui/DrawContext;I)V", at = @At("HEAD"), cancellable = true)
    void injectRenderExperienceBar(DrawContext context, int x, CallbackInfo ci){
        ci.cancel();
    }
}
