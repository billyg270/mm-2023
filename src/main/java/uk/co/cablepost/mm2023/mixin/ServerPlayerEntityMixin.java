package uk.co.cablepost.mm2023.mixin;

import net.fabricmc.fabric.api.networking.v1.PacketByteBufs;
import net.fabricmc.fabric.api.networking.v1.ServerPlayNetworking;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.server.network.ServerPlayerEntity;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import uk.co.cablepost.mm2023.Mm2023;

@Mixin(ServerPlayerEntity.class)
public class ServerPlayerEntityMixin {
    @Inject(method = "damage(Lnet/minecraft/entity/damage/DamageSource;F)Z", at = @At("RETURN"))
    public void inject(DamageSource source, float amount, CallbackInfoReturnable<Boolean> cir){
        if(cir.getReturnValue()){
            if(source.getAttacker() != null){
                ServerPlayerEntity thisObject = (ServerPlayerEntity)(Object)this;
                //{
                    PacketByteBuf buf = PacketByteBufs.create();
                    buf.writeInt(source.getAttacker().getId());
                    ServerPlayNetworking.send(thisObject, Mm2023.SET_LAST_DAMAGE_PACKET_ID, buf);
                //}
//                {
//                    if(source.getAttacker().isPlayer()){
//                        ServerPlayerEntity attackerPlayer = thisObject.getServerWorld().getPlayers(x -> x.getId() == source.getAttacker().getId()).get(0);
//
//                        PacketByteBuf buf = PacketByteBufs.create();
//                        buf.writeInt(thisObject.getId());
//                        ServerPlayNetworking.send(attackerPlayer, Mm2023.SET_LAST_ATTACK_PACKET_ID, buf);
//                    }
//                }
            }
        }
    }
}
