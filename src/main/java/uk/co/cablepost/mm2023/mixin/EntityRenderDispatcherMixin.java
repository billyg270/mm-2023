package uk.co.cablepost.mm2023.mixin;

import net.minecraft.client.render.entity.EntityRenderDispatcher;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import uk.co.cablepost.mm2023.client.enjaraiDummy.DummyClientPlayerEntity;

@Mixin(EntityRenderDispatcher.class)
public abstract class EntityRenderDispatcherMixin<T extends LivingEntity> {
    @Inject(
            method = "getSquaredDistanceToCamera(Lnet/minecraft/entity/Entity;)D",
            at = @At("HEAD"),
            cancellable = true
    )
    private void inject(Entity entity, CallbackInfoReturnable<Double> cir) {
        if (entity instanceof DummyClientPlayerEntity) {
            cir.setReturnValue(5d);
        }
    }
}
