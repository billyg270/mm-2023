package uk.co.cablepost.mm2023.mixin;

import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.network.ClientPlayerEntity;
import net.minecraft.entity.Entity;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.Vec3d;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import uk.co.cablepost.mm2023.Mm2023;
import uk.co.cablepost.mm2023.client.Mm2023Client;

@Environment(EnvType.CLIENT)
@Mixin(Entity.class)
public class ClientSideEntityMixin {

    @Inject(method = "changeLookDirection(DD)V", at = @At("HEAD"))
    private void injected(double cursorDeltaX, double cursorDeltaY, CallbackInfo ci) {
        ClientPlayerEntity player = MinecraftClient.getInstance().player;
        assert player != null;
        assert MinecraftClient.getInstance().world != null;

        boolean isSpectator = player.isSpectator();

        if(Mm2023Client.LAST_DAMAGED_BY == null || Mm2023Client.LAST_DAMAGED_BY == -1 || !isSpectator || player.getScoreboardTeam() == null) {
            return;
        }

        Entity lastAttacker = MinecraftClient.getInstance().world.getEntityById(Mm2023Client.LAST_DAMAGED_BY);

        if(lastAttacker == null) {
            return;
        }

        Mm2023.LOGGER.info("Looking towards " + lastAttacker.getName().getString());

        Vec3d vec3d = player.getEyePos();
        Vec3d target = lastAttacker.getEyePos();

        double d = target.x - vec3d.x;
        double e = target.y - vec3d.y;
        double f = target.z - vec3d.z;
        double g = Math.sqrt(d * d + f * f);

        float targetPitch = MathHelper.wrapDegrees((float)(-(MathHelper.atan2(e, g) * 57.2957763671875)));
        float targetYaw = MathHelper.wrapDegrees((float)(MathHelper.atan2(f, d) * 57.2957763671875) - 90.0f);

        float pitchDiff = targetPitch - player.getPitch();
        float yawDiff = targetYaw - player.getYaw();

        while (yawDiff > 180){
            yawDiff -= 360;
        }

        while (yawDiff < -180){
            yawDiff += 360;
        }

        pitchDiff *= 0.05f;
        if(pitchDiff > 0.8f){
            pitchDiff = 0.8f;
        }
        if(pitchDiff < -0.8f){
            pitchDiff = -0.8f;
        }

        yawDiff *= 0.1f;
        if(yawDiff > 2f){
            yawDiff = 2f;
        }
        if(yawDiff < -2f){
            yawDiff = -2f;
        }

        player.setPitch(player.getPitch() + pitchDiff);
        player.setYaw(player.getYaw() + yawDiff);
        player.setPitch(MathHelper.clamp(player.getPitch(), -90.0f, 90.0f));
        player.prevPitch += pitchDiff;
        player.prevYaw += yawDiff;
        player.prevPitch = MathHelper.clamp(player.prevPitch, -90.0f, 90.0f);
    }
}
