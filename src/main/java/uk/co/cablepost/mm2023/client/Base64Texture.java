package uk.co.cablepost.mm2023.client;

import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.resource.metadata.TextureResourceMetadata;
import net.minecraft.client.texture.NativeImage;
import net.minecraft.client.texture.ResourceTexture;
import net.minecraft.resource.ResourceManager;
import net.minecraft.util.Identifier;
import org.apache.commons.codec.binary.Base64;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

@Environment(value= EnvType.CLIENT)
public class Base64Texture extends ResourceTexture {

    Identifier ID;
    String base64Image;

    public Base64Texture(Identifier id, String base64Image) {
        super(id);
        this.ID = id;
        this.base64Image = base64Image;
    }

    @Override
    protected ResourceTexture.TextureData loadTextureData(ResourceManager resourceManager) {
        ResourceTexture.TextureData textureData;
        {
            Base64 decoder = new Base64();
            byte[] imgBytes = decoder.decode(base64Image);
            InputStream inputStream = new ByteArrayInputStream(imgBytes);

            try {
                textureData = new TextureData(new TextureResourceMetadata(true, true), NativeImage.read(inputStream));
            } catch (Throwable throwable) {
                try {
                    throw throwable;
                } catch (IOException iOException) {
                    return new TextureData(iOException);
                }
            }
            try {
                inputStream.close();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        return textureData;
    }
}