package uk.co.cablepost.mm2023.client;

import java.util.Arrays;

public class KeyboardInputs {
    boolean inputUp = false;
    boolean inputDown = false;
    boolean inputLeft = false;
    boolean inputRight = false;

    int[] upKeys = {265, 87};
    int[] downKeys = {264, 83};
    int[] leftKeys = {263, 65};
    int[] rightKeys = {262, 68};

    public KeyboardInputs(){}

    public void keyPressed(int keyCode) {
        updateInputs(keyCode, true);

    }

    public void keyReleased(int keyCode) {
        updateInputs(keyCode, false);
    }

    private void updateInputs(int keyCode, boolean pressed){
        if(Arrays.stream(upKeys).anyMatch(x -> x == keyCode)){
            inputUp = pressed;
        }
        if(Arrays.stream(downKeys).anyMatch(x -> x == keyCode)){
            inputDown = pressed;
        }
        if(Arrays.stream(leftKeys).anyMatch(x -> x == keyCode)){
            inputLeft = pressed;
        }
        if(Arrays.stream(rightKeys).anyMatch(x -> x == keyCode)){
            inputRight = pressed;
        }
    }

    public boolean getInputUp(){
        return inputUp;
    }

    public boolean getInputDown(){
        return inputDown;
    }

    public boolean getInputLeft(){
        return inputLeft;
    }

    public boolean getInputRight(){
        return inputRight;
    }
}
