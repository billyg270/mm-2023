package uk.co.cablepost.mm2023.client.screen;

import net.minecraft.client.MinecraftClient;
import net.minecraft.client.font.TextRenderer;
import net.minecraft.client.gui.DrawContext;
import net.minecraft.client.gui.screen.ConnectScreen;
import net.minecraft.client.gui.screen.Overlay;
import net.minecraft.client.gui.screen.multiplayer.MultiplayerScreen;
import net.minecraft.client.network.ServerAddress;
import net.minecraft.client.network.ServerInfo;
import net.minecraft.client.render.RenderLayer;
import net.minecraft.text.Text;
import net.minecraft.util.math.ColorHelper;
import uk.co.cablepost.mm2023.Mm2023;
import uk.co.cablepost.mm2023.client.Mm2023Client;

public class AuxiliaryGameOverlay extends Overlay {

    private static final int BACKGROUND_COLOR = ColorHelper.Argb.getArgb(255, 3, 168, 166);

    TextRenderer textRenderer;
    Boolean joiningServer;

    public AuxiliaryGameOverlay() {
        this.textRenderer = MinecraftClient.getInstance().textRenderer;
        this.joiningServer = false;
    }

    private void cancel(){// TODO - add button or something so players can cancel and return to main menu
        Mm2023Client.orchestratorApiClient.dequeue();
        MinecraftClient.getInstance().setOverlay(null);
    }

    private void tryJoinServer(){
        if(joiningServer || Mm2023Client.orchestratorApiClient.fetchingMinigameIp()){
            return;
        }
        joiningServer = true;
        joinServer();
    }

    private void joinServer(){
        ServerAddress serverAddress = ServerAddress.parse(Mm2023Client.orchestratorApiClient.getMinigameIp());
        ServerInfo serverInfo = new ServerInfo("Minigame Instance", Mm2023Client.orchestratorApiClient.getMinigameIp(), false);

        Mm2023.LOGGER.info("Will now connect to {}", serverAddress);
        ConnectScreen.connect(new MultiplayerScreen(new MmTitleScreen()), MinecraftClient.getInstance(), serverAddress, serverInfo, true);

        //MinecraftClient.getInstance().setOverlay(null);// Instead done when player receives packet that minigame is starting
    }

    @Override
    public void render(DrawContext context, int mouseX, int mouseY, float delta) {
        int width = context.getScaledWindowWidth();
        int height = context.getScaledWindowHeight();

        context.fill(RenderLayer.getGuiOverlay(), 0, 0, width, height, BACKGROUND_COLOR);

        context.drawTextWithShadow(
                this.textRenderer,
                Text.of("Up: " + (Mm2023Client.keyboardInputs.getInputUp() ? "Y" : "N")),
                10,
                10,
                0xFFFFFF
        );

        context.drawTextWithShadow(
                this.textRenderer,
                Text.of("Down: " + (Mm2023Client.keyboardInputs.getInputDown() ? "Y" : "N")),
                10,
                20,
                0xFFFFFF
        );

        context.drawTextWithShadow(
                this.textRenderer,
                Text.of("Left: " + (Mm2023Client.keyboardInputs.getInputLeft() ? "Y" : "N")),
                10,
                30,
                0xFFFFFF
        );

        context.drawTextWithShadow(
                this.textRenderer,
                Text.of("Right: " + (Mm2023Client.keyboardInputs.getInputRight() ? "Y" : "N")),
                10,
                40,
                0xFFFFFF
        );

        context.drawTextWithShadow(
                this.textRenderer,
                Text.of("IP: " + (Mm2023Client.orchestratorApiClient.getMinigameIp() == null ? "NULL" : Mm2023Client.orchestratorApiClient.getMinigameIp())),
                10,
                50,
                0xFFFFFF
        );

        tryJoinServer();
    }
}
