package uk.co.cablepost.mm2023.client;

import com.mojang.authlib.minecraft.MinecraftProfileTexture;
import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.fabric.api.client.event.lifecycle.v1.ClientLifecycleEvents;
import net.fabricmc.fabric.api.client.event.lifecycle.v1.ClientTickEvents;
import net.fabricmc.fabric.api.client.networking.v1.ClientPlayNetworking;
import net.fabricmc.loader.api.FabricLoader;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.gui.screen.ingame.HandledScreens;
import net.minecraft.util.Identifier;
import uk.co.cablepost.mm2023.Mm2023;
import uk.co.cablepost.mm2023.block.minigameUtil.markerBlock.MarkerBlockScreen;
import uk.co.cablepost.mm2023.client.screen.MmTitleScreen;
import uk.co.cablepost.mm2023.init.MinigameUtilBlockInit;
import uk.co.cablepost.mm2023.init.SwordInit;

import java.time.Instant;

@Environment(value = EnvType.CLIENT)
public class Mm2023Client implements ClientModInitializer {

    public static OrchestratorApiClient orchestratorApiClient = new OrchestratorApiClient();
    public static Identifier PLAYER_SKIN_IDENTIFIER = null;
    public static KeyboardInputs keyboardInputs = new KeyboardInputs();
    public static boolean OVERRIDE_TITLE_SCREEN = true;
    public static Integer LAST_DAMAGED_BY = null;
    public static Long LAST_DAMAGED_TIME = null;
    public static Integer LAST_ATTACKED_BY = null;
    public static Long LAST_ATTACKED_TIME = null;

    public static boolean CAN_MOVE = true;
    public static boolean CAN_LOOK = true;

    public static boolean B_INPUT = false;
    public static int B_INPUT_FORWARD = 0;
    public static int B_INPUT_SIDEWAYS = 0;
    public static boolean B_INPUT_JUMP = false;
    public static boolean B_INPUT_SNEAK = false;

    public static boolean DISCONNECT_NEXT_TICK = false;

    @Override
    public void onInitializeClient() {
        new SwordInit().clientInit();
        new MinigameUtilBlockInit().clientInit();

        if(!FabricLoader.getInstance().isDevelopmentEnvironment()) {
            orchestratorApiClient.Login();
        }

        if(orchestratorApiClient.getHasValidSession()){
            orchestratorApiClient.fetchLevel();
            ClientLifecycleEvents.CLIENT_STARTED.register((client) -> client.getSkinProvider().loadSkin(client.getSession().getProfile(), (type, identifier, texture) -> {
                if (type == MinecraftProfileTexture.Type.SKIN) {
                    Mm2023.LOGGER.info("Skin loaded! " + identifier.toString());
                    PLAYER_SKIN_IDENTIFIER = identifier;
                }
            }, false));
        }
        else{
            Mm2023.LOGGER.warn("Failed to login to API");
        }

        ClientPlayNetworking.registerGlobalReceiver(Mm2023.SET_LAST_DAMAGE_PACKET_ID, (client, handler, buf, responseSender) -> {
            assert MinecraftClient.getInstance().world != null;
            LAST_DAMAGED_BY = buf.readInt();
            LAST_DAMAGED_TIME = Instant.now().toEpochMilli();
        });

        ClientPlayNetworking.registerGlobalReceiver(Mm2023.SET_LAST_ATTACK_PACKET_ID, (client, handler, buf, responseSender) -> {
            assert MinecraftClient.getInstance().world != null;
            LAST_ATTACKED_BY = buf.readInt();
            LAST_ATTACKED_TIME = Instant.now().toEpochMilli();
        });

        ClientPlayNetworking.registerGlobalReceiver(Mm2023.HIDE_AUX_GAME_PACKET_ID, (client, handler, buf, responseSender) -> {
            MinecraftClient.getInstance().setOverlay(null);
        });

        ClientPlayNetworking.registerGlobalReceiver(Mm2023.MINIGAME_OVER_PACKET_ID, (client, handler, buf, responseSender) -> {
            DISCONNECT_NEXT_TICK = true;
        });

        ClientPlayNetworking.registerGlobalReceiver(Mm2023.SET_INPUTS_ALLOWED_PACKET_ID, (client, handler, buf, responseSender) -> {
            CAN_MOVE = buf.readBoolean();
            CAN_LOOK = buf.readBoolean();
        });

        ClientTickEvents.START_CLIENT_TICK.register((client) -> {
            if(DISCONNECT_NEXT_TICK){
                DISCONNECT_NEXT_TICK = false;
                client.disconnect();
                client.setScreen(new MmTitleScreen());
            }
        });
    }
}
