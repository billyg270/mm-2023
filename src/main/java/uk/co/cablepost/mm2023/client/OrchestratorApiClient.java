package uk.co.cablepost.mm2023.client;

import com.mojang.authlib.minecraft.client.ObjectMapper;
import com.mojang.authlib.yggdrasil.request.JoinMinecraftServerRequest;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.MinecraftClient;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import uk.co.cablepost.mm2023.Mm2023;
import uk.co.cablepost.mm2023.client.model.OrchestratorApiClientLoginParamsModel;

import java.io.IOException;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.security.SecureRandom;

@Environment(value= EnvType.CLIENT)
public class OrchestratorApiClient {
    private Boolean validSession = false;
    private String apiToken = null;
    private FetchMinigameIpThread fetchMinigameIpThread = null;

    private float level = -1.0f;

    public void Login(){
        byte[] bytes = new byte[20];
        new SecureRandom().nextBytes(bytes);

        String sharedSecret = new BigInteger(bytes).toString(16);

        //Mm2023.LOGGER.info(sharedSecret);

        MinecraftClient mcClient = MinecraftClient.getInstance();

        JoinMinecraftServerRequest sessionServerRequest = new JoinMinecraftServerRequest();

        sessionServerRequest.serverId = sharedSecret;
        sessionServerRequest.selectedProfile = mcClient.getSession().getUuidOrNull();
        sessionServerRequest.accessToken = mcClient.getSession().getAccessToken();

        HttpClient httpClient = HttpClientBuilder.create().build();

        try {
            HttpPost request = new HttpPost("https://sessionserver.mojang.com/session/minecraft/join");
            StringEntity params = new StringEntity(ObjectMapper.create().writeValueAsString(sessionServerRequest));
            request.addHeader("content-type", "application/json");
            request.setEntity(params);
            HttpResponse response = httpClient.execute(request);
            validSession = response.getStatusLine().getStatusCode() == HttpURLConnection.HTTP_NO_CONTENT;
        } catch (Exception e) {
            Mm2023.LOGGER.error("Failed to validate session: " + e.getMessage());
        }

        Mm2023.LOGGER.info("Valid MC session: " + validSession);

        try {
            HttpPost request = new HttpPost(Mm2023.API_BASE_URL + "Client/Login");
            StringEntity params = new StringEntity(ObjectMapper.create().writeValueAsString(new OrchestratorApiClientLoginParamsModel(
                    mcClient.getSession().getUuidOrNull().toString(),
                    mcClient.getSession().getUsername(),
                    sharedSecret
            )));
            request.addHeader("content-type", "application/json");
            request.setEntity(params);
            HttpResponse response = httpClient.execute(request);
            apiToken = EntityUtils.toString(response.getEntity(), "UTF-8");
        } catch (Exception e) {
            Mm2023.LOGGER.error("Failed to login to API: " + e.getMessage());
        }

        //Mm2023.LOGGER.info("API Token: " + apiToken);

        PlayerHeartbeatThread playerHeartbeatThread = new PlayerHeartbeatThread(apiToken);
        playerHeartbeatThread.start();
    }

    public boolean getHasValidSession(){
        return validSession;
    }

    public float getLevel(){
        return level;
    }

    public void fetchLevel(){
        HttpClient httpClient = HttpClientBuilder.create().build();
        HttpGet request = new HttpGet(Mm2023.API_BASE_URL + "playerStats/GetLevel?playerUuid=" + MinecraftClient.getInstance().getSession().getUuidOrNull().toString());
        try {
            HttpResponse response = httpClient.execute(request);
            this.level = Float.parseFloat(EntityUtils.toString(response.getEntity(), "UTF-8"));
        } catch (IOException e) {
            throw new RuntimeException("Unable to get player's level: " + e);
        }
    }

    public void startFetchMinigameIp(String minigame){
        Mm2023.LOGGER.info("Starting search for minigame server for: " + minigame);
        if(fetchMinigameIpThread != null){
            fetchMinigameIpThread.scheduleEnd = true;
        }

        fetchMinigameIpThread = new FetchMinigameIpThread(minigame, apiToken);
        fetchMinigameIpThread.start();
    }

    public Boolean fetchingMinigameIp(){
        return fetchMinigameIpThread != null && fetchMinigameIpThread.isAlive();
    }

    public String getMinigameIp(){
        if(fetchMinigameIpThread == null){
            throw new RuntimeException("Trying to get minigame IP from thread but thread is null");
        }

        return fetchMinigameIpThread.serverIp;
    }

    public void dequeue(){
        if(fetchMinigameIpThread != null){
            fetchMinigameIpThread.scheduleEnd = true;
        }

        HttpClient httpClient = HttpClientBuilder.create().build();
        HttpGet request = new HttpGet(Mm2023.API_BASE_URL + "PlayMinigame/Dequeue");
        request.addHeader("playerUuid", MinecraftClient.getInstance().getSession().getUuidOrNull().toString());
        request.addHeader("token", apiToken);
        try {
            httpClient.execute(request);
        } catch (IOException e) {
            throw new RuntimeException("Unable to get Minigame IP: " + e);
        }
    }
}

class FetchMinigameIpThread extends Thread {
    public boolean scheduleEnd = false;
    public String serverIp = null;
    private final String minigame;
    private String serverName = null;
    private final String apiToken;

    public FetchMinigameIpThread(String minigame, String apiToken){
        this.minigame = minigame;
        this.apiToken = apiToken;
    }

    @Override
    public void run() {
        while(true) {
            Mm2023.LOGGER.info("Running search for minigame server for: " + minigame);

            if(scheduleEnd){
                Mm2023.LOGGER.info("Ending search for minigame server for: " + minigame);
                break;
            }

            HttpClient httpClient = HttpClientBuilder.create().build();
            if(serverName == null){
                HttpGet request = new HttpGet(Mm2023.API_BASE_URL + "PlayMinigame/Queue?minigame=" + minigame);
                request.addHeader("playerUuid", MinecraftClient.getInstance().getSession().getUuidOrNull().toString());
                request.addHeader("token", apiToken);
                try {
                    HttpResponse response = httpClient.execute(request);
                    if (response.getStatusLine().getStatusCode() == HttpURLConnection.HTTP_OK){
                        serverName = EntityUtils.toString(response.getEntity(), "UTF-8");
                        Mm2023.LOGGER.info("[Get server name] Concluding search for minigame server for: " + minigame + " Server name: " + serverName);
                    }
                    else{
                        Mm2023.LOGGER.info("[Get server name] Run result for search for minigame server for " + minigame + ": " + EntityUtils.toString(response.getEntity(), "UTF-8"));
                    }
                } catch (IOException e) {
                    throw new RuntimeException("[Get server name] Unable to get Minigame Server Name: " + e);
                }
            }
            else{
                HttpGet request = new HttpGet(Mm2023.API_BASE_URL + "PlayMinigame/GetServerIp");
                request.addHeader("playerUuid", MinecraftClient.getInstance().getSession().getUuidOrNull().toString());
                request.addHeader("token", apiToken);
                try {
                    HttpResponse response = httpClient.execute(request);
                    if (response.getStatusLine().getStatusCode() == HttpURLConnection.HTTP_OK){
                        serverIp = EntityUtils.toString(response.getEntity(), "UTF-8");
                        Mm2023.LOGGER.info("[Get server IP] Concluding search for minigame server for: " + minigame + " Server IP: " + serverIp);
                        break;
                    }
                    else{
                        Mm2023.LOGGER.info("[Get server IP] Run result for search for minigame server IP for " + minigame + ": " + EntityUtils.toString(response.getEntity(), "UTF-8"));
                    }
                } catch (IOException e) {
                    throw new RuntimeException("[Get server IP] Unable to get Minigame Server IP: " + e);
                }
            }

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }
}


class PlayerHeartbeatThread extends Thread {
    private final String apiToken;
    private final String playerUuid;

    public PlayerHeartbeatThread(String apiToken){
        this.apiToken = apiToken;
        this.playerUuid = MinecraftClient.getInstance().getSession().getUuidOrNull().toString();
    }

    @Override
    public void run() {
        while(true) {
            HttpClient httpClient = HttpClientBuilder.create().build();

            HttpGet request = new HttpGet(Mm2023.API_BASE_URL + "Client/Heartbeat");
            request.addHeader("playerUuid", playerUuid);
            request.addHeader("token", apiToken);
            try {
                httpClient.execute(request);
            } catch (IOException e) {
                throw new RuntimeException("Unable to send client heartbeat: " + e);
            }

            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
