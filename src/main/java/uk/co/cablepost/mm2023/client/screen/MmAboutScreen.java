package uk.co.cablepost.mm2023.client.screen;

import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.gui.DrawContext;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.screen.option.OptionsScreen;
import net.minecraft.client.gui.widget.ButtonWidget;
import net.minecraft.text.Text;

@Environment(value = EnvType.CLIENT)
public class MmAboutScreen extends Screen {

    public ButtonWidget backButton;

    protected MmAboutScreen() {
        super(Text.of("About"));
    }

    @Override
    public void init(){
        backButton = ButtonWidget.builder(Text.of("Back"), button -> {
                    assert client != null;
                    client.setScreen(new MmTitleScreen());
                })
                .dimensions(width - 110, height - 30, 100, 20)
                .build()
        ;
        addDrawableChild(backButton);
    }

    @Override
    public void render(DrawContext context, int mouseX, int mouseY, float delta) {
        this.renderBackground(context);
        super.render(context, mouseX, mouseY, delta);

        context.drawTextWithShadow(
                this.textRenderer,
                Text.of("About"),
                10,
                10,
                0xFFFFFF
        );
    }
}
