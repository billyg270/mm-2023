package uk.co.cablepost.mm2023.client.screen;

import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.gui.DrawContext;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.screen.TitleScreen;
import net.minecraft.client.gui.screen.ingame.InventoryScreen;
import net.minecraft.client.gui.screen.option.OptionsScreen;
import net.minecraft.client.gui.widget.ButtonWidget;
import net.minecraft.client.toast.SystemToast;
import net.minecraft.client.toast.ToastManager;
import net.minecraft.client.util.DefaultSkinHelper;
import net.minecraft.text.Text;
import net.minecraft.util.Identifier;
import uk.co.cablepost.mm2023.Mm2023;
import uk.co.cablepost.mm2023.client.Mm2023Client;
import uk.co.cablepost.mm2023.client.enjaraiDummy.DummyClientPlayerEntity;

@Environment(value = EnvType.CLIENT)
public class MmTitleScreen extends Screen {

    public static final Identifier LOGO_TEXTURE = new Identifier("textures/gui/title/minecraft.png");
    public static final Identifier BACKGROUND_TEXTURE = new Identifier(Mm2023.MOD_ID, "textures/gui/background.png");
    public static final Identifier CABLEPOST_TEXTURE = new Identifier(Mm2023.MOD_ID, "textures/gui/cablepost.png");

    DummyClientPlayerEntity player = null;

    public ButtonWidget friendsButton;
    public ButtonWidget partyButton;
    public ButtonWidget trophiesButton;

    public ButtonWidget oneVsOnePvpMinigameButton;
    public ButtonWidget captureTheFlagMinigameButton;

    public ButtonWidget optionsButton;
    public ButtonWidget aboutButton;
    public ButtonWidget quitButton;
    public ButtonWidget toggleTitleScreenButton;

    public MmTitleScreen() {
        super(Text.translatable("narrator.screen.mm_title"));
    }

    @Override
    public void init() {
        //player = getDummyPlayer();

        int topStart = (int)Math.floor(height * 0.4f) - 20;

        int rightColumnStart = ((int) Math.floor(height * 0.4f) + 20);
        int rightColumnWidth = Math.round((float)(width - rightColumnStart) / 3f) * 3;
        int rightColumnWidthThird = Math.round((float)rightColumnWidth / 3f);

        friendsButton = ButtonWidget.builder(Text.of("Friends"), button -> {
                    assert client != null;
                    ToastManager toastManager = MinecraftClient.getInstance().getToastManager();
                    SystemToast.show(toastManager, SystemToast.Type.NARRATOR_TOGGLE, Text.of("Coming soon!"), null);
                })
                .dimensions(rightColumnStart, topStart, rightColumnWidthThird - 10, 20)
                .build()
        ;
        addDrawableChild(friendsButton);

        partyButton = ButtonWidget.builder(Text.of("Party"), button -> {
                    assert client != null;
                    ToastManager toastManager = MinecraftClient.getInstance().getToastManager();
                    SystemToast.show(toastManager, SystemToast.Type.NARRATOR_TOGGLE, Text.of("Coming soon!"), null);
                })
                .dimensions(rightColumnStart + rightColumnWidthThird, topStart, rightColumnWidthThird - 10, 20)
                .build()
        ;
        addDrawableChild(partyButton);

        trophiesButton = ButtonWidget.builder(Text.of("Trophies"), button -> {
                    assert client != null;
                    ToastManager toastManager = MinecraftClient.getInstance().getToastManager();
                    SystemToast.show(toastManager, SystemToast.Type.NARRATOR_TOGGLE, Text.of("Coming soon!"), null);
                })
                .dimensions(rightColumnStart + rightColumnWidthThird * 2, topStart, rightColumnWidthThird - 10, 20)
                .build()
        ;
        addDrawableChild(trophiesButton);

        oneVsOnePvpMinigameButton = ButtonWidget.builder(Text.of("1v1 PVP"), button -> {
                    assert client != null;
                    if(Mm2023Client.orchestratorApiClient.getHasValidSession()) {
                        Mm2023Client.orchestratorApiClient.startFetchMinigameIp("1v1PVP");
                        client.setOverlay(new AuxiliaryGameOverlay());
                    }
                    else{
                        ToastManager toastManager = MinecraftClient.getInstance().getToastManager();
                        SystemToast.show(toastManager, SystemToast.Type.NARRATOR_TOGGLE, Text.of("Must be logged in to do this"), null);
                    }
                })
                .dimensions(rightColumnStart, topStart + 30, rightColumnWidth - 10, 30)
                .build()
        ;
        addDrawableChild(oneVsOnePvpMinigameButton);

        captureTheFlagMinigameButton = ButtonWidget.builder(Text.of("Capture the Flag"), button -> {
                    assert client != null;
                    ToastManager toastManager = MinecraftClient.getInstance().getToastManager();
                    SystemToast.show(toastManager, SystemToast.Type.NARRATOR_TOGGLE, Text.of("Coming soon!"), null);
                })
                .dimensions(rightColumnStart, topStart + 70, rightColumnWidth - 10, 30)
                .build()
        ;
        addDrawableChild(captureTheFlagMinigameButton);

        optionsButton = ButtonWidget.builder(Text.translatable("menu.options"), button -> {
                    assert client != null;
                    client.setScreen(new OptionsScreen(this, this.client.options));
                })
                .dimensions(rightColumnStart, height - 40, rightColumnWidthThird - 10, 20)
                .build()
        ;
        addDrawableChild(optionsButton);

        aboutButton = ButtonWidget.builder(Text.of("About"), button -> {
                    assert client != null;
                    client.setScreen(new MmAboutScreen());
                })
                .dimensions(rightColumnStart + rightColumnWidthThird, height - 40, rightColumnWidthThird - 10, 20)
                .build()
        ;
        addDrawableChild(aboutButton);

        quitButton = ButtonWidget.builder(Text.translatable("menu.quit"), button -> {
                    assert client != null;
                    this.client.scheduleStop();
                })
                .dimensions(rightColumnStart + rightColumnWidthThird * 2, height - 40, rightColumnWidthThird - 10, 20)
                .build()
        ;
        addDrawableChild(quitButton);

        toggleTitleScreenButton = ButtonWidget.builder(Text.of("..."), button -> {
                    assert client != null;
                    Mm2023Client.OVERRIDE_TITLE_SCREEN = false;
                    client.setScreen(new TitleScreen(false));
                })
                .dimensions(width - 20, height - 20, 10, 10)
                .build()
        ;
        addDrawableChild(toggleTitleScreenButton);
    }

    public DummyClientPlayerEntity getDummyPlayer() {
        assert client != null;

        if(Mm2023Client.PLAYER_SKIN_IDENTIFIER == null){
            return null;
        }

        Mm2023.LOGGER.info(client.getSkinProvider().loadSkin(client.getSession().getProfile()).toString());
        return new DummyClientPlayerEntity(
                null,
                client.getSession().getUuidOrNull(),
                Mm2023Client.PLAYER_SKIN_IDENTIFIER,
                ""
        );
    }

    @Override
    public void renderBackground(DrawContext context) {
        context.drawTexture(BACKGROUND_TEXTURE, 0, 0, 0.0f, 0.0f, 1000, 600, width, height);
    }

    @Override
    public void render(DrawContext context, int mouseX, int mouseY, float delta) {
        this.renderBackground(context);
        if(player == null){
            player = getDummyPlayer();
        }

        super.render(context, mouseX, mouseY, delta);

        context.drawTexture(LOGO_TEXTURE, 10, 20, 0.0f, 0.0f, 256, 44, 256, 64);

        assert client != null;
        Text playerNameText = Text.of(client.getSession().getUsername());
        context.drawTextWithShadow(
                this.textRenderer,
                playerNameText,
                (int)Math.floor(height * 0.05f) + (int)Math.floor(height * 0.35f / 2f) - textRenderer.getWidth(playerNameText) / 2,
                (int)Math.floor(height * 0.4f) - 20,
                0xFFFFFF
        );
        if(player != null) {
            InventoryScreen.drawEntity(
                    context,
                    (int) Math.floor(height * 0.23f),
                    (int) Math.floor(height * 0.68f),
                    32,
                    (int) Math.floor(height * 0.23f) - mouseX,
                    (int) Math.floor(height * 0.68f) - mouseY,
                    player
            );
        }

        Text levelText = Text.of("Level: " + Mm2023Client.orchestratorApiClient.getLevel());
        context.drawTextWithShadow(
                this.textRenderer,
                levelText,
                (int)Math.floor(height * 0.05f) + (int)Math.floor(height * 0.35f / 2f) - textRenderer.getWidth(levelText) / 2,
                (int)Math.floor(height * 0.7f) + 20,
                0xFFFFFF
        );

        context.drawTexture(
                CABLEPOST_TEXTURE,
                width - 10 - (int) Math.floor(height * 0.08f * 69f/38f),
                10,
                0.0f,
                0.0f,
                (int) Math.floor(height * 0.08f * 69f/38f),
                (int) Math.floor(height * 0.08f),
                (int) Math.floor(height * 0.08f * 69f/38f),
                (int) Math.floor(height * 0.08f)
        );

        context.drawTextWithShadow(this.textRenderer, "Mini Madness version 0.1", 2, this.height - 10, 0xFFFFFF);
        context.drawTextWithShadow(this.textRenderer, TitleScreen.COPYRIGHT, width - textRenderer.getWidth(TitleScreen.COPYRIGHT) - 2, this.height - 10, 0xFFFFFF);
    }

    @Override
    public boolean shouldPause() {
        return false;
    }

    @Override
    public boolean shouldCloseOnEsc() {
        return false;
    }
}
