package uk.co.cablepost.mm2023.client.model;

import com.google.gson.annotations.SerializedName;

public record OrchestratorApiClientLoginParamsModel (
    @SerializedName("playerUuid")
    String PlayerUuid,
    @SerializedName("username")
    String Username,

    @SerializedName("sharedSecret")
    String SharedSecret
){}
