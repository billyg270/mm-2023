package uk.co.cablepost.mm2023.server;

import uk.co.cablepost.mm2023.block.minigameUtil.markerBlock.MarkerData;
import uk.co.cablepost.mm2023.server.minigame.Minigame;
import uk.co.cablepost.mm2023.server.minigame.OneVsOnePvpMinigame;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MmStaticServerData {
    public static Map<String, MarkerData> MARKERS = new HashMap<>();
    public static boolean SPECTATOR_ON_DEATH = false;// Set by minigame instance
    public static boolean DISABLE_MOVE_ON_DEATH = false;// Set by minigame instance
    public static boolean DISABLE_LOOK_ON_DEATH = false;// Set by minigame instance
    public static OrchestratorApiClientForServer ORCHESTRATOR_API_CLIENT_FOR_SERVER = null;
    public static List<String> CONNECTED_PLAYERS = new ArrayList<>();
    public static Map<String, ConnectingPlayerModel> CONNECTING_PLAYERS = new HashMap<>();// Also holds connected players

    /*
    BeforeGame = 0,
    InGame = 1,
    AfterGame = 2
    */
    public static int MINIGAME_STATUS = 0;
    public static String SERVER_NAME = null;
    public static String GAMEMODE = null;
    public static String MAP_NAME = null;
    public static long MAP_X = 0;
    public static long MAP_Y = 0;
    public static long MAP_Z = 0;
    public static int TEAM_COUNT = 0;
    public static int MIN_TEAM_PLAYERS = 0;
    public static int MAX_TEAM_PLAYERS = 0;
    public static Minigame MINIGAME_INSTANCE = null;

    public static final Map<String, Class<? extends Minigame>> MINIGAMES = Map.of(// Should match what's on maps table in DB
            "1v1PVP", OneVsOnePvpMinigame.class
    );

    public static final String[] TEAM_NAMES = {// Should match what's on API
        "red",
        "blue",
        "green",
        "yellow"
    };
}
