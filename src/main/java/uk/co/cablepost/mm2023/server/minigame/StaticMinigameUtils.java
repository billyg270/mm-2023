package uk.co.cablepost.mm2023.server.minigame;

import net.fabricmc.fabric.api.networking.v1.PacketByteBufs;
import net.fabricmc.fabric.api.networking.v1.ServerPlayNetworking;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.network.packet.s2c.play.TitleFadeS2CPacket;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.server.world.ServerWorld;
import uk.co.cablepost.mm2023.Mm2023;

import java.util.List;

public class StaticMinigameUtils {
    // Make static methods here for common functions

    public static List<ServerPlayerEntity> getPlayersInTeams(ServerWorld world){
        //return world.getPlayers(x -> MmStaticServerData.CONNECTING_PLAYERS.containsKey(x.getGameProfile().getId().toString()) && MmStaticServerData.CONNECTING_PLAYERS.get(x.getGameProfile().getId().toString()).teamToJoin != null);
        return world.getPlayers(x -> x.getScoreboardTeam() != null);
    }

    public static void setTitleTimes(ServerWorld world, int fadeInTicks, int stayTicks, int fadeOutTicks){
        TitleFadeS2CPacket titleFadeS2CPacket = new TitleFadeS2CPacket(fadeInTicks, stayTicks, fadeOutTicks);
        world.getServer().getPlayerManager().sendToAll(titleFadeS2CPacket);
    }

    public static void resetTitleTimes(ServerWorld world){
        setTitleTimes(world, 10, 70, 20);
    }

    public static void setInputsAllowed(ServerWorld world, boolean canMove, boolean canLook){
        for (var player : getPlayersInTeams(world)) {
            setInputsAllowed(player, canMove, canLook);
        }
    }

    public static void setInputsAllowed(ServerPlayerEntity player, boolean canMove, boolean canLook){
        PacketByteBuf buf = PacketByteBufs.create();
        buf.writeBoolean(canMove);
        buf.writeBoolean(canLook);
        ServerPlayNetworking.send(player, Mm2023.SET_INPUTS_ALLOWED_PACKET_ID, buf);
    }
}
