package uk.co.cablepost.mm2023.server;

import com.google.gson.annotations.SerializedName;

public record OrchestratorApiServerHeartbeatParamsModel (
    @SerializedName("token")
    String Token,
    @SerializedName("playerUuidsConnected")
    String[] PlayerUuidsConnected,
    @SerializedName("status")
    int Status
){}
