package uk.co.cablepost.mm2023.server.minigame;

import net.fabricmc.fabric.api.networking.v1.PacketByteBufs;
import net.fabricmc.fabric.api.networking.v1.ServerPlayNetworking;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.scoreboard.AbstractTeam;
import net.minecraft.scoreboard.Team;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.text.Text;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.GameRules;
import net.minecraft.world.World;
import uk.co.cablepost.mm2023.Mm2023;
import uk.co.cablepost.mm2023.server.ConnectingPlayerModel;
import uk.co.cablepost.mm2023.server.MmStaticServerData;

import java.util.ArrayList;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;

public abstract class Minigame {

    public Minigame(){}

    private boolean doneOnInit = false;
    private int ticksSinceFirstPlayerJoined = 0;
    private boolean doneOnInitOncePlayerJoined = false;
    private int ticksSinceGameFinished = 0;

    public abstract void onInit(ServerWorld world);

    public void onTick(ServerWorld world){
        if(world.getRegistryKey().getValue() != World.OVERWORLD.getValue()){
            return;
        }

        if(!doneOnInit){
            Mm2023.LOGGER.info("Minigame loaded");
            doneOnInit = true;
            world.setSpawnPos(new BlockPos((int)MmStaticServerData.MAP_X, (int)MmStaticServerData.MAP_Y, (int)MmStaticServerData.MAP_Z), 0f);
            world.getServer().getGameRules().get(GameRules.SPAWN_RADIUS).set(0, world.getServer());

            MmStaticServerData.SPECTATOR_ON_DEATH = true;
            MmStaticServerData.DISABLE_LOOK_ON_DEATH = true;
            MmStaticServerData.DISABLE_MOVE_ON_DEATH = true;

            world.getServer().getGameRules().get(GameRules.ANNOUNCE_ADVANCEMENTS).set(false, world.getServer());
            world.getServer().getGameRules().get(GameRules.DISABLE_ELYTRA_MOVEMENT_CHECK).set(true, world.getServer());
            world.getServer().getGameRules().get(GameRules.SPECTATORS_GENERATE_CHUNKS).set(false, world.getServer());
            world.getServer().getGameRules().get(GameRules.DO_LIMITED_CRAFTING).set(true, world.getServer());

            for(var p : world.getPlayers()){
                p.teleport((int)MmStaticServerData.MAP_X, (int)MmStaticServerData.MAP_Y, (int)MmStaticServerData.MAP_Z);
            }
        }

        if(doneOnInit && !doneOnInitOncePlayerJoined){
            if((long) world.getPlayers().size() > 0) {
                ticksSinceFirstPlayerJoined++;

                if(ticksSinceFirstPlayerJoined % 10 == 0) {
                    for (var p : world.getPlayers()) {
                        p.teleport((int) MmStaticServerData.MAP_X, (int) MmStaticServerData.MAP_Y, (int) MmStaticServerData.MAP_Z);
                    }
                }

                if(ticksSinceFirstPlayerJoined >= 100) {
                    Mm2023.LOGGER.info("Minigame loaded #2");
                    doneOnInitOncePlayerJoined = true;

                    onInit(world);
                }
            }
        }

        if(doneOnInit && doneOnInitOncePlayerJoined) {
            if (MmStaticServerData.MINIGAME_STATUS == 0) {
                beforeGameTick(world);
            } else if (MmStaticServerData.MINIGAME_STATUS == 1) {
                inGameTick(world);
            } else if (MmStaticServerData.MINIGAME_STATUS == 2) {
                afterGameTick(world);
            } else {
                throw new RuntimeException("Unknown MINIGAME_STATUS: " + MmStaticServerData.MINIGAME_STATUS);
            }
        }
    }

    void beforeGameTick(ServerWorld world){
        ArrayList<ArrayList<String>> teams = new ArrayList<>();
        for(int i = 0; i < MmStaticServerData.TEAM_COUNT; i++){
            teams.add(new ArrayList<>());
        }

        // Check everyone who is joining has joined
        var players = world.getPlayers();
        for(Map.Entry<String, ConnectingPlayerModel> entry : MmStaticServerData.CONNECTING_PLAYERS.entrySet()){
            if(players.stream().noneMatch(x -> Objects.equals(x.getGameProfile().getId().toString(), entry.getKey()))){
                // One of the players in the CONNECTING_PLAYERS obj is not in game yet
                return;
            }

            teams.get(entry.getValue().teamToJoin).add(entry.getKey());
        }

        // Check teams are full enough to start
        for (ArrayList<String> teamPlayer : teams) {
            if (teamPlayer.size() < MmStaticServerData.MIN_TEAM_PLAYERS) {
                // Not enough players in this team to start
                return;
            }
        }


        // Assign players to MC teams and start game
        for (int teamIndex = 0; teamIndex < teams.size(); teamIndex++) {
            ArrayList<String> teamPlayers = teams.get(teamIndex);

            Team team = world.getScoreboard().getTeam(MmStaticServerData.TEAM_NAMES[teamIndex]);
            assert team != null;

            for(int teamPlayerIndex = 0; teamPlayerIndex < teamPlayers.size(); teamPlayerIndex++){
                PlayerEntity player = world.getPlayerByUuid(UUID.fromString(teamPlayers.get(teamPlayerIndex)));
                assert player != null;

                world.getScoreboard().addPlayerToTeam(player.getUuidAsString(), team);
            }

            team.setCollisionRule(AbstractTeam.CollisionRule.NEVER);
        }

        onGameStart(world);

        for(ServerPlayerEntity player : world.getPlayers()) {
            ServerPlayNetworking.send(player, Mm2023.HIDE_AUX_GAME_PACKET_ID, PacketByteBufs.empty());
            StaticMinigameUtils.setInputsAllowed(player, true, true);
        }
        MmStaticServerData.MINIGAME_STATUS = 1;
    }

    abstract void onGameStart(ServerWorld world);

    abstract void inGameTick(ServerWorld world);

    void afterGameTick(ServerWorld world){
        ticksSinceGameFinished ++;

        if(ticksSinceGameFinished == 100){
            // Send everyone to main menu gracefully
            for (var player : world.getPlayers()) {
                PacketByteBuf buf = PacketByteBufs.create();
                ServerPlayNetworking.send(player, Mm2023.MINIGAME_OVER_PACKET_ID, buf);
            }
        }

        if(ticksSinceGameFinished >= 500) {
            // Kick everyone
            for (var player : world.getPlayers()) {
                player.networkHandler.disconnect(Text.of("Minigame finished"));
            }
        }
    }
}
