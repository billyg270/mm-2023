package uk.co.cablepost.mm2023.server;

import com.google.gson.annotations.SerializedName;

import java.util.Map;

public class HeartbeatRespModel {
    @SerializedName("gamemode")
    public String Gamemode;

    @SerializedName("mapName")
    public String MapName;

    @SerializedName("mapX")
    public long MapX;

    @SerializedName("mapY")
    public long MapY;

    @SerializedName("mapZ")
    public long MapZ;

    @SerializedName("players")
    public Map<String, Integer> Players;

    @SerializedName("teams")
    public int Teams;

    @SerializedName("minTeamPlayers")
    public int MinTeamPlayers;

    @SerializedName("maxTeamPlayers")
    public int MaxTeamPlayers;
}
