package uk.co.cablepost.mm2023.server.minigame;

import net.minecraft.block.Block;
import net.minecraft.block.Blocks;
import net.minecraft.enchantment.Enchantments;
import net.minecraft.entity.effect.StatusEffectInstance;
import net.minecraft.entity.effect.StatusEffects;
import net.minecraft.item.DyeableArmorItem;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.network.packet.s2c.play.GameMessageS2CPacket;
import net.minecraft.network.packet.s2c.play.TitleS2CPacket;
import net.minecraft.scoreboard.AbstractTeam;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.text.Text;
import net.minecraft.util.Formatting;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.GameMode;
import uk.co.cablepost.mm2023.Mm2023;
import uk.co.cablepost.mm2023.block.minigameUtil.markerBlock.MarkerData;
import uk.co.cablepost.mm2023.server.MmStaticServerData;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

public class OneVsOnePvpMinigame extends Minigame {
    private final ArrayList<String> arenasDone = new ArrayList<>();
    private int roundStartCountdownTicks = 0;
    private int roundEndCountdownTicks = 0;
    private final HashMap<String, MarkerData> arenaRedSpawnMarkers = new HashMap<>();
    private final HashMap<String, MarkerData> arenaBlueSpawnMarkers = new HashMap<>();

    @Override
    public void onInit(ServerWorld world) {
        for(var marker : MmStaticServerData.MARKERS.entrySet()){
            // Spawn marker format: mapName-arenaName-red-0.5-1-0.5
            if(marker.getValue().tag.toLowerCase().startsWith(MmStaticServerData.MAP_NAME.toLowerCase() + "-")){
                String arenaName = marker.getValue().tag.toLowerCase().split("-")[1];
                String spawnTeam = marker.getValue().tag.toLowerCase().split("-")[2];

                if(spawnTeam.equalsIgnoreCase("red")){
                    arenaRedSpawnMarkers.put(arenaName, marker.getValue());
                }
                else if(spawnTeam.equalsIgnoreCase("blue")){
                    arenaBlueSpawnMarkers.put(arenaName, marker.getValue());
                }
                else {
                    throw new RuntimeException("Unknown team for 1v1PVP: " + spawnTeam);
                }
            }
        }

        Mm2023.LOGGER.info("onInit complete for 1v1PVP");
    }

    @Override
    void onGameStart(ServerWorld world) {
        for(var p : StaticMinigameUtils.getPlayersInTeams(world)) {
            AbstractTeam team = p.getScoreboardTeam();
            if (team == null) {
                continue;
            }

            p.getInventory().clear();

            // Get color
            Formatting color = team.getColor();
            Integer teamColor = null;
            if (color != null) {
                teamColor = color.getColorValue();
            }

            // Add armour
            ItemStack helmet = new ItemStack(Items.LEATHER_HELMET, 1);
            helmet.addEnchantment(Enchantments.BINDING_CURSE, 1);

            if (teamColor != null) {
                ((DyeableArmorItem) Items.LEATHER_HELMET.asItem()).setColor(helmet, teamColor);
            }

            p.getInventory().armor.set(3, helmet);

            ItemStack chestplate = new ItemStack(Items.LEATHER_CHESTPLATE, 1);
            chestplate.addEnchantment(Enchantments.BINDING_CURSE, 1);

            if (teamColor != null) {
                ((DyeableArmorItem) Items.LEATHER_CHESTPLATE.asItem()).setColor(chestplate, teamColor);
            }

            p.getInventory().armor.set(2, chestplate);

            ItemStack leggings = new ItemStack(Items.IRON_LEGGINGS, 1);
            leggings.addEnchantment(Enchantments.BINDING_CURSE, 1);

            p.getInventory().armor.set(1, leggings);

            ItemStack boots = new ItemStack(Items.IRON_BOOTS, 1);
            boots.addEnchantment(Enchantments.BINDING_CURSE, 1);

            p.getInventory().armor.set(0, boots);

            ItemStack sword = new ItemStack(Items.IRON_SWORD, 1);

            p.getInventory().setStack(0, sword);
        }

        startRound(world);
    }

    // Selects a random arena and adds it to the done list so not picked again
    private String selectArena(ServerWorld world){
        while(true){
            String[] arenaNames = arenaRedSpawnMarkers.keySet().toArray(new String[0]);
            String arenaName = arenaNames[world.random.nextInt(arenaNames.length)];

            if(!arenasDone.contains(arenaName)){
                arenasDone.add(arenaName);
                return arenaName;
            }
        }
    }

    private void startRound(ServerWorld world){
        String arenaName = selectArena(world);

        roundStartCountdownTicks = 20 * 5;

        // List players in teams (not spectators)
        for(var player : StaticMinigameUtils.getPlayersInTeams(world)){
            var p = Objects.requireNonNull(world.getServer()).getPlayerManager().getPlayer(player.getUuid());
            assert p != null;
            p.setHealth(20);
            p.setFireTicks(0);
            p.clearStatusEffects();
            p.changeGameMode(GameMode.ADVENTURE);
            StaticMinigameUtils.setInputsAllowed(p, true, true);
            p.addStatusEffect(new StatusEffectInstance(StatusEffects.RESISTANCE, 2, 255, true, false));
            p.addStatusEffect(new StatusEffectInstance(StatusEffects.REGENERATION, 2, 255, true, false));

            String teamName = Objects.requireNonNull(player.getScoreboardTeam()).getName();

            MarkerData spawnPoint;
            MarkerData otherSpawnPoint;
            if(teamName.equalsIgnoreCase("red")){
                spawnPoint = arenaRedSpawnMarkers.get(arenaName);
                otherSpawnPoint = arenaBlueSpawnMarkers.get(arenaName);
            }
            else{
                spawnPoint = arenaBlueSpawnMarkers.get(arenaName);
                otherSpawnPoint = arenaRedSpawnMarkers.get(arenaName);
            }

            String[] spawnTagSplit = spawnPoint.tag.split("-");

            float offsetX =  Float.parseFloat(spawnTagSplit[3]);
            float offsetY =  Float.parseFloat(spawnTagSplit[4]);
            float offsetZ =  Float.parseFloat(spawnTagSplit[5]);

            player.teleport(
                    world,
                    spawnPoint.blockPos.getX() + offsetX,
                    spawnPoint.blockPos.getY() + offsetY + 1.1f,
                    spawnPoint.blockPos.getZ() + offsetZ,
                    (float)Math.toDegrees(Math.atan2(
                            otherSpawnPoint.blockPos.getX() - spawnPoint.blockPos.getX(),
                            otherSpawnPoint.blockPos.getZ() - spawnPoint.blockPos.getZ()
                    )),
                    0f
            );
        }

        StaticMinigameUtils.resetTitleTimes(world);
        world.getServer().getPlayerManager().sendToAll(new GameMessageS2CPacket(Text.of("Round " + arenasDone.size()), true));
        world.getServer().getPlayerManager().sendToAll(new TitleS2CPacket(Text.of("Round " + arenasDone.size())));
    }

    @Override
    void inGameTick(ServerWorld world) {
        // Before round
        if(roundStartCountdownTicks > 0){
            roundStartCountdownTicks--;

            if(roundStartCountdownTicks == 20 * 3){
                StaticMinigameUtils.setTitleTimes(world, 5, 10, 5);
                world.getServer().getPlayerManager().sendToAll(new TitleS2CPacket(Text.literal("3")));
            }
            else if(roundStartCountdownTicks == 20 * 2){
                world.getServer().getPlayerManager().sendToAll(new TitleS2CPacket(Text.literal("2")));
            }
            else if(roundStartCountdownTicks == 20){
                world.getServer().getPlayerManager().sendToAll(new TitleS2CPacket(Text.literal("1")));
            }
            else if(roundStartCountdownTicks == 0){
                world.getServer().getPlayerManager().sendToAll(new TitleS2CPacket(Text.literal("Fight")));

                for(var player : StaticMinigameUtils.getPlayersInTeams(world)){
                    removeGlassPanesAroundPos(world, player.getBlockPos());
                }
            }

            return;
        }

        // After round
        if(roundEndCountdownTicks > 0){
            for(var player : StaticMinigameUtils.getPlayersInTeams(world)){
                player.addStatusEffect(new StatusEffectInstance(StatusEffects.RESISTANCE, 2, 255, true, false));
                player.addStatusEffect(new StatusEffectInstance(StatusEffects.REGENERATION, 2, 255, true, false));
            }

            roundEndCountdownTicks--;
            if(roundEndCountdownTicks == 0){
                if(arenasDone.size() >= 5){
                    // Game over
                    world.getServer().getPlayerManager().sendToAll(new TitleS2CPacket(Text.literal("Game over!")));
                    MmStaticServerData.MINIGAME_STATUS = 2;
                    return;
                }

                startRound(world);
            }

            return;
        }

        // In round
        int alivePlayers = 0;
        for(ServerPlayerEntity player : StaticMinigameUtils.getPlayersInTeams(world)){
            if(player.interactionManager.getGameMode() != GameMode.SPECTATOR){
                alivePlayers ++;
            }
        }

        if(alivePlayers == 1){
            for(ServerPlayerEntity player : StaticMinigameUtils.getPlayersInTeams(world)) {
                if(player.interactionManager.getGameMode() != GameMode.SPECTATOR) {
                    world.getServer().getPlayerManager().sendToAll(new GameMessageS2CPacket(
                        Text.literal("").append(player.getDisplayName()).append(Text.of(" wins round " + arenasDone.size())),
                        false
                    ));
                }
            }

            roundEndCountdownTicks = 20 * 3;
        }
        else if(alivePlayers == 0){
            world.getServer().getPlayerManager().sendToAll(new GameMessageS2CPacket(
                Text.of("No one wins round " + arenasDone.size()),
                false
            ));

            roundEndCountdownTicks = 20 * 3;
        }
    }

    private void removeGlassPanesAroundPos(ServerWorld world, BlockPos blockPos){
        for(int x = -8; x <= 8; x++){
            for(int y = -8; y <= 8; y++){
                for(int z = -8; z <= 8; z++){
                    if(world.getBlockState(blockPos.add(x, y, z)).getBlock().getTranslationKey().contains("glass_pane")) {
                        world.setBlockState(blockPos.add(x, y, z), Blocks.AIR.getDefaultState(), Block.NOTIFY_ALL);
                    }
                }
            }
        }
    }
}
