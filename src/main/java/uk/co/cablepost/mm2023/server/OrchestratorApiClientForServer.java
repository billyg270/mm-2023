package uk.co.cablepost.mm2023.server;

import com.mojang.authlib.minecraft.client.ObjectMapper;
import uk.co.cablepost.mm2023.Mm2023;
import uk.co.cablepost.mm2023.server.minigame.Minigame;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;
import java.util.Date;
import java.util.Map;

public class OrchestratorApiClientForServer {
    private final String apiKey;

    public OrchestratorApiClientForServer(String apiKey){
        this.apiKey = apiKey;

        Mm2023.LOGGER.info("Starting to send heartbeats to API as server");

        ServerHeartbeatThread thread = new ServerHeartbeatThread(this.apiKey);
        thread.start();
    }


}

class ServerHeartbeatThread extends Thread {
    private final String apiToken;

    public ServerHeartbeatThread(String apiToken){
        this.apiToken = apiToken;
    }

    @Override
    public void run() {
        while(true) {
            HttpClient httpClient = HttpClient.newHttpClient();

            String heartbeatRequestParams = ObjectMapper.create().writeValueAsString(new OrchestratorApiServerHeartbeatParamsModel(
                apiToken,
                MmStaticServerData.CONNECTED_PLAYERS.toArray(new String[0]),
                MmStaticServerData.MINIGAME_STATUS
            ));

            HttpRequest heartbeatRequest = HttpRequest.newBuilder()
                .uri(URI.create(Mm2023.API_BASE_URL + "InternalMcServer/Heartbeat"))
                .timeout(Duration.ofMinutes(10))
                .header("Content-Type", "application/json")
                .POST(HttpRequest.BodyPublishers.ofString(heartbeatRequestParams))
                .build()
            ;

            try {
                HttpResponse<String> heartbeatResponse = httpClient.send(heartbeatRequest, HttpResponse.BodyHandlers.ofString());

                HeartbeatRespModel resp = ObjectMapper.create().readValue(heartbeatResponse.body(), HeartbeatRespModel.class);

                /*
                System.out.println("--------------------------------------------");
                System.out.println("Map name: " + resp.MapName);
                System.out.println("Gamemode: " + resp.Gamemode);
                System.out.println("Map X: " + resp.MapX);
                System.out.println("Map Y: " + resp.MapY);
                System.out.println("Map Z: " + resp.MapZ);
                System.out.println("Team count: " + resp.Teams);
                System.out.println("Min team players: " + resp.MinTeamPlayers);
                System.out.println("Max team players: " + resp.MaxTeamPlayers);
                System.out.println("Connecting players: " + resp.Players.entrySet().size());
                for(var c : resp.Players.entrySet()){
                    System.out.println("- Connecting player: " + c.getKey() + " - " + c.getValue().toString());
                }
                System.out.println("--------------------------------------------");
                */

                MmStaticServerData.MAP_NAME = resp.MapName;
                MmStaticServerData.MAP_X = resp.MapX;
                MmStaticServerData.MAP_Y = resp.MapY;
                MmStaticServerData.MAP_Z = resp.MapZ;
                MmStaticServerData.TEAM_COUNT = resp.Teams;
                MmStaticServerData.MIN_TEAM_PLAYERS = resp.MinTeamPlayers;
                MmStaticServerData.MAX_TEAM_PLAYERS = resp.MaxTeamPlayers;

                if(MmStaticServerData.GAMEMODE == null && resp.Gamemode != null){
                    try {
                        MmStaticServerData.MINIGAME_INSTANCE = MmStaticServerData.MINIGAMES.get(resp.Gamemode).getConstructor().newInstance();
                    } catch (
                            NoSuchMethodException |
                            InvocationTargetException |
                            InstantiationException |
                            IllegalAccessException
                            e
                    ) {
                        throw new RuntimeException("Unable to start minigame instance: " + e);
                    }
                }

                MmStaticServerData.GAMEMODE = resp.Gamemode;

                // Add any to local set new from API resp + update team assignments
                for(Map.Entry<String, Integer> p : resp.Players.entrySet()){
                    if(!MmStaticServerData.CONNECTING_PLAYERS.containsKey(p.getKey())){
                        MmStaticServerData.CONNECTING_PLAYERS.put(p.getKey(), new ConnectingPlayerModel(p.getValue()));
                    }
                    else{
                        MmStaticServerData.CONNECTING_PLAYERS.get(p.getKey()).teamToJoin = p.getValue();
                    }

                    // Dequeue players who have not joined in 30 seconds from first added to set
                    if(
                            !MmStaticServerData.CONNECTED_PLAYERS.contains(p.getKey()) &&
                            MmStaticServerData.CONNECTING_PLAYERS.get(p.getKey()).startedConnectingTimestamp.before(new Date(System.currentTimeMillis() - 30 * 1000))
                    ){
                        HttpRequest dequeueRequest = HttpRequest.newBuilder()
                                .uri(URI.create(Mm2023.API_BASE_URL + "InternalMcServer/DequeuePlayer?playerUuid=" + p.getKey() + "&fromServer=" + MmStaticServerData.SERVER_NAME))
                                .timeout(Duration.ofMinutes(10))
                                .header("token", apiToken)
                                .GET()
                                .build()
                        ;

                        try {
                            httpClient.send(dequeueRequest, HttpResponse.BodyHandlers.ofString());
                            MmStaticServerData.CONNECTING_PLAYERS.remove(p.getKey());
                            Mm2023.LOGGER.info("Dequeued " + p.getKey() + " as they did not join for 30 seconds");
                        } catch (IOException | InterruptedException e) {
                            Mm2023.LOGGER.warn("Failed to call API to dequeue player, will try again");
                        }
                    }
                }

                // Remove any in local set not in API resp
                for(Map.Entry<String, ConnectingPlayerModel> p : MmStaticServerData.CONNECTING_PLAYERS.entrySet()){
                    if(!resp.Players.containsKey(p.getKey())){
                        MmStaticServerData.CONNECTING_PLAYERS.remove(p.getKey());
                    }
                }
            } catch (IOException | InterruptedException e) {
                Mm2023.LOGGER.warn("Failed to heartbeat API, will try again");
            }

            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
