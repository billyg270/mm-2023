package uk.co.cablepost.mm2023.server;

import java.util.Date;

public class ConnectingPlayerModel {
    public Date startedConnectingTimestamp;
    public Integer teamToJoin;

    public ConnectingPlayerModel(Integer teamToJoin){
        startedConnectingTimestamp = new Date();
        this.teamToJoin = teamToJoin;
    }
}
