package uk.co.cablepost.mm2023.block.minigameUtil.markerBlock;

import net.fabricmc.fabric.api.screenhandler.v1.ExtendedScreenHandlerFactory;
import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.network.listener.ClientPlayPacketListener;
import net.minecraft.network.packet.Packet;
import net.minecraft.network.packet.s2c.play.BlockEntityUpdateS2CPacket;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.text.Text;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import org.jetbrains.annotations.Nullable;
import uk.co.cablepost.mm2023.Mm2023;
import uk.co.cablepost.mm2023.init.MinigameUtilBlockInit;
import uk.co.cablepost.mm2023.server.MmStaticServerData;

import java.util.Objects;

public class MarkerBlockEntity extends BlockEntity implements ExtendedScreenHandlerFactory {
    public String tag = "";

    public MarkerBlockEntity(BlockPos pos, BlockState state) {
        super(MinigameUtilBlockInit.MARKER_BLOCK_ENTITY, pos, state);
    }

    @Override
    public void writeNbt(NbtCompound nbt) {
        nbt.putString("tag", tag);
        super.writeNbt(nbt);
    }

    @Override
    public void readNbt(NbtCompound nbt) {
        super.readNbt(nbt);
        tag = nbt.getString("tag");
    }

    @Nullable
    @Override
    public Packet<ClientPlayPacketListener> toUpdatePacket() {
        return BlockEntityUpdateS2CPacket.create(this);
    }

    @Override
    public NbtCompound toInitialChunkDataNbt() {
        return createNbt();
    }

    public static void tick(World world, BlockPos pos, BlockState state, MarkerBlockEntity be) {
        if (world.isClient) {
            return;
        }

        if(MmStaticServerData.MARKERS.containsKey(pos.toString())){
            if(!Objects.equals(MmStaticServerData.MARKERS.get(pos.toString()).tag, be.tag)) {
                Mm2023.LOGGER.info("Updated marker: " + MmStaticServerData.MARKERS.get(pos.toString()).tag + " to " + be.tag);
                MmStaticServerData.MARKERS.get(pos.toString()).tag = be.tag;
                markDirty(world, pos, state);
            }
        }
        else{
            Mm2023.LOGGER.info("Added marker: " + be.tag);
            MmStaticServerData.MARKERS.put(pos.toString(), new MarkerData(pos, be.tag));
        }
    }

    @Override
    public void markRemoved() {
        if(MmStaticServerData.MARKERS.containsKey(pos.toString())) {
            Mm2023.LOGGER.info("Removed marker: " + MmStaticServerData.MARKERS.get(pos.toString()).tag);
            MmStaticServerData.MARKERS.remove(pos.toString());
        }
        super.markRemoved();
    }

    @Override
    public void writeScreenOpeningData(ServerPlayerEntity player, PacketByteBuf buf) {
        buf.writeString(tag);
    }

    @Override
    public Text getDisplayName() {
        return Text.translatable(getCachedState().getBlock().getTranslationKey());
    }

    @Nullable
    @Override
    public ScreenHandler createMenu(int syncId, PlayerInventory playerInventory, PlayerEntity player) {
        return new MarkerBlockScreenHandler(syncId, playerInventory, pos, world);
    }
}
