package uk.co.cablepost.mm2023.block.minigameUtil.teamBarrierBlock;

import net.minecraft.block.*;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.block.entity.BlockEntityTicker;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.client.item.TooltipContext;
import net.minecraft.item.ItemPlacementContext;
import net.minecraft.item.ItemStack;
import net.minecraft.particle.ParticleTypes;
import net.minecraft.state.StateManager;
import net.minecraft.state.property.DirectionProperty;
import net.minecraft.state.property.IntProperty;
import net.minecraft.text.Text;
import net.minecraft.util.Formatting;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.util.shape.VoxelShape;
import net.minecraft.util.shape.VoxelShapes;
import net.minecraft.world.BlockView;
import net.minecraft.world.World;
import org.jetbrains.annotations.Nullable;
import uk.co.cablepost.mm2023.init.MinigameUtilBlockInit;
import uk.co.cablepost.mm2023.server.MmStaticServerData;

import java.util.List;


public class TeamBarrierBlock extends BlockWithEntity {
    public static final IntProperty TEAM_INDEX = IntProperty.of("team_index", 0, MmStaticServerData.TEAM_NAMES.length);
    public static final DirectionProperty FACING = HorizontalFacingBlock.FACING;

    public TeamBarrierBlock(Settings settings) {
        super(settings);
        this.setDefaultState(this.stateManager.getDefaultState().with(FACING, Direction.NORTH).with(TEAM_INDEX, 0));
    }

    @Override
    public BlockState getPlacementState(ItemPlacementContext ctx) {
        String offhandItemName = (ctx.getPlayer() != null && ctx.getPlayer().getOffHandStack() == null) ? "none" : ctx.getPlayer().getOffHandStack().getName().toString();
        BlockState state = this.getDefaultState().with(FACING, ctx.getHorizontalPlayerFacing().getOpposite());
        for(int i = 0; i < MmStaticServerData.TEAM_NAMES.length; i++) {
            if(offhandItemName.toLowerCase().contains(MmStaticServerData.TEAM_NAMES[i].toLowerCase())){
                return state.with(TEAM_INDEX, i + 1);
            }
        }

        return state.with(TEAM_INDEX, 0);
    }

    @Nullable
    @Override
    public BlockEntity createBlockEntity(BlockPos pos, BlockState state) {
        return new TeamBarrierBlockEntity(pos, state);
    }

    @Nullable
    public <T extends BlockEntity> BlockEntityTicker<T> getTicker(World world, BlockState state, BlockEntityType<T> type) {
        return world.isClient ? checkType(type, MinigameUtilBlockInit.TEAM_BARRIER_BLOCK_ENTITY, TeamBarrierBlockEntity::clientTick) : checkType(type, MinigameUtilBlockInit.TEAM_BARRIER_BLOCK_ENTITY, TeamBarrierBlockEntity::serverTick);
    }

    @Override
    public BlockRenderType getRenderType(BlockState state) {
        return BlockRenderType.MODEL;
    }

    @Override
    protected void appendProperties(StateManager.Builder<Block, BlockState> builder) {
        builder.add(FACING).add(TEAM_INDEX);
    }

    @Override
    public VoxelShape getCameraCollisionShape(BlockState state, BlockView world, BlockPos pos, ShapeContext context) {
        return VoxelShapes.empty();
    }

    @Override
    public float getAmbientOcclusionLightLevel(BlockState state, BlockView world, BlockPos pos) {
        return 1.0f;
    }

    @Override
    public boolean isTransparent(BlockState state, BlockView world, BlockPos pos) {
        return true;
    }

    @Override
    public void randomDisplayTick(BlockState state, World world, BlockPos pos, net.minecraft.util.math.random.Random random)
    {
        Direction direction = state.get(TeamBarrierBlock.FACING);

        double blockX = (double)pos.getX() + 0.5;
        double blockY = pos.getY();
        double blockZ = (double)pos.getZ() + 0.5;
        double xOffset = random.nextDouble() * 6.0 / 16.0;
        double yOffset = random.nextDouble() * 6.0 / 16.0;
        double zOffset = random.nextDouble() * 6.0 / 16.0;

        world.addParticle(
                ParticleTypes.SOUL,
                blockX + xOffset,
                blockY + yOffset,
                blockZ + zOffset,
                direction.getOffsetX() * 0.1f,
                direction.getOffsetY() * 0.1f,
                direction.getOffsetZ() * 0.1f
        );
    }

    @Override
    public void appendTooltip(ItemStack itemStack, BlockView world, List<Text> tooltip, TooltipContext tooltipContext) {
        tooltip.add( Text.literal("Hold dye in offhand for team color").formatted(Formatting.ITALIC, Formatting.GREEN) );
    }

}
