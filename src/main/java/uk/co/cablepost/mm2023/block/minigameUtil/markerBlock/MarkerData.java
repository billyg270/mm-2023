package uk.co.cablepost.mm2023.block.minigameUtil.markerBlock;

import net.minecraft.util.math.BlockPos;

public class MarkerData {
    public BlockPos blockPos;
    public String tag;

    public MarkerData(BlockPos bp, String t){
        blockPos = bp;
        tag = t;
    }

    @Override
    public String toString() {
        return "Block Pos: " + blockPos.toString() + ", Tag: " + tag;
    }
}
