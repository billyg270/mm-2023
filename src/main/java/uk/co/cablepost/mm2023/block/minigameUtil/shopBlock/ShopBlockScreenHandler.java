package uk.co.cablepost.mm2023.block.minigameUtil.shopBlock;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.screen.slot.Slot;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import uk.co.cablepost.mm2023.init.MinigameUtilBlockInit;

public class ShopBlockScreenHandler extends ScreenHandler {

    public ShopBlockScreenHandler(int syncId, PlayerInventory playerInventory, PacketByteBuf buf) {
        this(syncId, playerInventory, null, null);
    }

    public ShopBlockScreenHandler(int syncId, PlayerInventory playerInventory, BlockPos blockPos, World world) {
        super(MinigameUtilBlockInit.SHOP_BLOCK_SCREEN_HANDLER, syncId);

        for (int k = 0; k < 3; ++k) {
            for (int l = 0; l < 9; ++l) {
                this.addSlot(new Slot(playerInventory, l + k * 9 + 9, 36 + l * 18, 137 + k * 18));
            }
        }
        for (int k = 0; k < 9; ++k) {
            this.addSlot(new Slot(playerInventory, k, 36 + k * 18, 195));
        }
    }

    @Override
    public ItemStack quickMove(PlayerEntity player, int slot) {
        return null;
    }

    @Override
    public boolean canUse(PlayerEntity player) {
        return true;
    }
}
