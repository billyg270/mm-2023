package uk.co.cablepost.mm2023.block.minigameUtil.markerBlock;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import uk.co.cablepost.mm2023.init.MinigameUtilBlockInit;

public class MarkerBlockScreenHandler extends ScreenHandler {

    private final World world;
    private final BlockPos pos;

    private String tag;

    public MarkerBlockScreenHandler(int syncId, PlayerInventory playerInventory, PacketByteBuf buf) {
        this(syncId, playerInventory, null, null);
        tag = buf.readString();
    }

    public MarkerBlockScreenHandler(int syncId, PlayerInventory playerInventory, BlockPos blockPos, World world) {
        super(MinigameUtilBlockInit.MARKER_BLOCK_SCREEN_HANDLER, syncId);
        tag = "";
        pos = blockPos;
        this.world = world;
    }

    @Override
    public ItemStack quickMove(PlayerEntity player, int slot) {
        return null;
    }

    @Override
    public boolean canUse(PlayerEntity player) {
        return true;
    }

    public String getTag(){
        return tag;
    }

    public void setTag(String t){
        tag = t;

        MarkerBlockEntity blockEntity = (MarkerBlockEntity)world.getBlockEntity(pos);
        assert blockEntity != null;
        blockEntity.tag = tag;
    }
}
