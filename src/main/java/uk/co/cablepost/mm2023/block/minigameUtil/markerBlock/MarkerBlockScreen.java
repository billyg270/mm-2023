package uk.co.cablepost.mm2023.block.minigameUtil.markerBlock;

import net.fabricmc.fabric.api.client.networking.v1.ClientPlayNetworking;
import net.fabricmc.fabric.api.networking.v1.PacketByteBufs;
import net.minecraft.client.gui.DrawContext;
import net.minecraft.client.gui.screen.ingame.HandledScreen;
import net.minecraft.client.gui.widget.TextFieldWidget;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.text.Text;
import net.minecraft.util.Identifier;
import uk.co.cablepost.mm2023.Mm2023;

public class MarkerBlockScreen extends HandledScreen<MarkerBlockScreenHandler> {
    private static final Identifier TEXTURE = new Identifier(Mm2023.MOD_ID, "textures/gui/container/marker.png");

    private TextFieldWidget tagField;
    private String tag = "???";

    public MarkerBlockScreen(MarkerBlockScreenHandler handler, PlayerInventory inventory, Text title) {
        super(handler, inventory, title);
    }

    @Override
    protected void init() {
        super.init();

        playerInventoryTitleX = -1000;

        if (handler instanceof MarkerBlockScreenHandler) {
            tag = handler.getTag();
        }

        int i = (this.width - this.backgroundWidth) / 2;
        int j = (this.height - this.backgroundHeight) / 2;
        tagField = new TextFieldWidget(this.textRenderer, i + 37, j + 38, 103, 12, Text.translatable("tooltip.mm2023.marker_tag_field"));
        tagField.setFocusUnlocked(false);
        tagField.setMaxLength(50);
        tagField.setText(tag);
        tagField.setChangedListener(this::onTypeTag);
        tagField.setEditable(true);
        tagField.setEditable(true);
        tagField.setEditableColor(-1);
        tagField.setUneditableColor(-1);
        tagField.setDrawsBackground(false);
        addSelectableChild(tagField);
        setInitialFocus(tagField);
    }

    private void onTypeTag(String newTag) {
        tag = newTag;
        SendMarkerUpdatePacket();
    }

    @Override
    protected void drawBackground(DrawContext context, float delta, int mouseX, int mouseY) {
        int x = (width - backgroundWidth) / 2;
        int y = (height - backgroundHeight) / 2;
        context.drawTexture(TEXTURE, x, y, 0, 0, backgroundWidth, backgroundHeight);
        tagField.render(context, mouseX, mouseY, delta);
        drawMouseoverTooltip(context, mouseX, mouseY);
    }

    private void SendMarkerUpdatePacket(){
        PacketByteBuf buf = PacketByteBufs.create();
        buf.writeString(tag);
        ClientPlayNetworking.send(new Identifier(Mm2023.MOD_ID, "update_marker"), buf);
    }
}