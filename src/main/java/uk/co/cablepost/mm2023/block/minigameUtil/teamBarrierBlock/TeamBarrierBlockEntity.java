package uk.co.cablepost.mm2023.block.minigameUtil.teamBarrierBlock;

import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Box;
import net.minecraft.util.math.Direction;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import uk.co.cablepost.mm2023.init.MinigameUtilBlockInit;
import uk.co.cablepost.mm2023.server.MmStaticServerData;

import java.util.List;


public class TeamBarrierBlockEntity extends BlockEntity {
    public TeamBarrierBlockEntity(BlockPos pos, BlockState state) {
        super(MinigameUtilBlockInit.TEAM_BARRIER_BLOCK_ENTITY, pos, state);
    }

    public static void clientTick(World world, BlockPos pos, BlockState state, TeamBarrierBlockEntity blockEntity) {
        serverTick(world, pos, state, blockEntity);
    }

    public static void serverTick(World world, BlockPos pos, BlockState state, TeamBarrierBlockEntity blockEntity) {
        Direction direction = state.get(TeamBarrierBlock.FACING);
        List<Entity> entities = world.getOtherEntities(null,
                new Box(
                        pos.getX() - 0.5f,
                        pos.getY() - 0.5f,
                        pos.getZ() - 0.5f,
                        pos.getX() + 1.5f,
                        pos.getY() + 1.5f,
                        pos.getZ() + 1.5f
                )
        );

        for (Entity entity : entities){
            if(!canEntityPassThrough(entity, state)){
                entity.setVelocity(new Vec3d(
                        direction.getOffsetX() * 0.1f,
                        direction.getOffsetY() * 0.1f,
                        direction.getOffsetZ() * 0.1f
                ));
            }
        }
    }

    private static boolean canEntityPassThrough(Entity entity, BlockState state){
        if(entity.isPlayer() && (((PlayerEntity)entity).isCreative()) || entity.isSpectator()){
            return true;
        }

        if(entity.getScoreboardTeam() == null){
            return false;
        }

        String blockTeam;
        int blockTeamIndex = state.get(TeamBarrierBlock.TEAM_INDEX);

        if(blockTeamIndex != 0){
            blockTeam = MmStaticServerData.TEAM_NAMES[blockTeamIndex - 1];
        }
        else{
            return true;
        }

        return entity.getScoreboardTeam().getName().equalsIgnoreCase(blockTeam);
    }
}
