package uk.co.cablepost.mm2023.mixinFunc;

import net.minecraft.client.MinecraftClient;
import net.minecraft.client.gui.screen.TitleScreen;
import uk.co.cablepost.mm2023.client.Mm2023Client;

public class MinecraftClientMixinFunc {
    public static boolean injectScheduleStop() {
        if(MinecraftClient.getInstance().currentScreen instanceof TitleScreen) {
            Mm2023Client.OVERRIDE_TITLE_SCREEN = true;
            return true;
        }

        if(Mm2023Client.orchestratorApiClient.getHasValidSession()){
            Mm2023Client.orchestratorApiClient.dequeue();
        }

        return false;
    }
}
