package uk.co.cablepost.mm2023.mixinFunc;

import com.google.common.collect.Ordering;
import com.mojang.blaze3d.systems.RenderSystem;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.gui.DrawContext;
import net.minecraft.client.gui.hud.InGameHud;
import net.minecraft.client.gui.screen.ingame.InventoryScreen;
import net.minecraft.client.texture.Sprite;
import net.minecraft.client.texture.StatusEffectSpriteManager;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.attribute.EntityAttributes;
import net.minecraft.entity.effect.StatusEffect;
import net.minecraft.entity.effect.StatusEffectInstance;
import net.minecraft.entity.effect.StatusEffectUtil;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.text.Text;
import net.minecraft.util.Arm;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.MathHelper;
import org.jetbrains.annotations.NotNull;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import uk.co.cablepost.mm2023.Mm2023;
import uk.co.cablepost.mm2023.client.Mm2023Client;

import java.time.Instant;
import java.util.Collection;

@Environment(value = EnvType.CLIENT)
public class InGameHudMixinFunc {

    private static final Identifier HOTBAR_SLOT_LEFT = new Identifier(Mm2023.MOD_ID, "textures/gui/sprites/hotbar_slot_left.png");
    private static final Identifier HOTBAR_SLOT_RIGHT = new Identifier(Mm2023.MOD_ID, "textures/gui/sprites/hotbar_slot_right.png");
    private static final Identifier HOTBAR_SELECTED_SLOT_LEFT = new Identifier(Mm2023.MOD_ID, "textures/gui/sprites/hotbar_selected_slot_left.png");
    private static final Identifier HOTBAR_SELECTED_SLOT_RIGHT = new Identifier(Mm2023.MOD_ID, "textures/gui/sprites/hotbar_selected_slot_right.png");
    private static final Identifier HOTBAR_OFFHAND_SLOT_LEFT = new Identifier(Mm2023.MOD_ID, "textures/gui/sprites/hotbar_offhand_slot_left.png");
    private static final Identifier HOTBAR_OFFHAND_SLOT_RIGHT = new Identifier(Mm2023.MOD_ID, "textures/gui/sprites/hotbar_offhand_slot_right.png");
    private static final Identifier HOTBAR_SLOT_MIDDLE = new Identifier(Mm2023.MOD_ID, "textures/gui/sprites/hotbar_slot_middle.png");

    private static final Identifier HEALTH_BAR_EMPTY_LEFT = new Identifier(Mm2023.MOD_ID, "textures/gui/sprites/health_bar_empty_left.png");
    private static final Identifier HEALTH_BAR_QUARTER_LEFT = new Identifier(Mm2023.MOD_ID, "textures/gui/sprites/health_bar_quarter_left.png");
    private static final Identifier HEALTH_BAR_HALF_LEFT = new Identifier(Mm2023.MOD_ID, "textures/gui/sprites/health_bar_half_left.png");
    private static final Identifier HEALTH_BAR_THREE_QUARTERS_LEFT = new Identifier(Mm2023.MOD_ID, "textures/gui/sprites/health_bar_three_quarters_left.png");
    private static final Identifier HEALTH_BAR_FULL_LEFT = new Identifier(Mm2023.MOD_ID, "textures/gui/sprites/health_bar_full_left.png");

    private static final Identifier HEALTH_BAR_EMPTY_RIGHT = new Identifier(Mm2023.MOD_ID, "textures/gui/sprites/health_bar_empty_right.png");
    private static final Identifier HEALTH_BAR_QUARTER_RIGHT = new Identifier(Mm2023.MOD_ID, "textures/gui/sprites/health_bar_quarter_right.png");
    private static final Identifier HEALTH_BAR_HALF_RIGHT = new Identifier(Mm2023.MOD_ID, "textures/gui/sprites/health_bar_half_right.png");
    private static final Identifier HEALTH_BAR_THREE_QUARTERS_RIGHT = new Identifier(Mm2023.MOD_ID, "textures/gui/sprites/health_bar_three_quarters_right.png");
    private static final Identifier HEALTH_BAR_FULL_RIGHT = new Identifier(Mm2023.MOD_ID, "textures/gui/sprites/health_bar_full_right.png");

    private static final Identifier ABSORPTION_BAR_EMPTY_LEFT = new Identifier(Mm2023.MOD_ID, "textures/gui/sprites/absorption_bar_empty_left.png");
    private static final Identifier ABSORPTION_BAR_QUARTER_LEFT = new Identifier(Mm2023.MOD_ID, "textures/gui/sprites/absorption_bar_quarter_left.png");
    private static final Identifier ABSORPTION_BAR_HALF_LEFT = new Identifier(Mm2023.MOD_ID, "textures/gui/sprites/absorption_bar_half_left.png");
    private static final Identifier ABSORPTION_BAR_THREE_QUARTERS_LEFT = new Identifier(Mm2023.MOD_ID, "textures/gui/sprites/absorption_bar_three_quarters_left.png");
    private static final Identifier ABSORPTION_BAR_FULL_LEFT = new Identifier(Mm2023.MOD_ID, "textures/gui/sprites/absorption_bar_full_left.png");

    private static final Identifier ABSORPTION_BAR_EMPTY_RIGHT = new Identifier(Mm2023.MOD_ID, "textures/gui/sprites/absorption_bar_empty_right.png");
    private static final Identifier ABSORPTION_BAR_QUARTER_RIGHT = new Identifier(Mm2023.MOD_ID, "textures/gui/sprites/absorption_bar_quarter_right.png");
    private static final Identifier ABSORPTION_BAR_HALF_RIGHT = new Identifier(Mm2023.MOD_ID, "textures/gui/sprites/absorption_bar_half_right.png");
    private static final Identifier ABSORPTION_BAR_THREE_QUARTERS_RIGHT = new Identifier(Mm2023.MOD_ID, "textures/gui/sprites/absorption_bar_three_quarters_right.png");
    private static final Identifier ABSORPTION_BAR_FULL_RIGHT = new Identifier(Mm2023.MOD_ID, "textures/gui/sprites/absorption_bar_full_right.png");

    private static final Identifier EFFECT_BACKGROUND_TEXTURE = new Identifier(Mm2023.MOD_ID, "textures/gui/sprites/button.png");

    public static void injectRenderStatusBars(DrawContext context, CallbackInfo ci){
        MinecraftClient client = MinecraftClient.getInstance();
        if (client.player == null) {
            return;
        }

        float otherHealth = 0;
        float otherAbsorption = 0;
        float otherMaxHealth = 0;

        Entity other = null;
        long now = Instant.now().toEpochMilli();
        if(Mm2023Client.LAST_DAMAGED_BY != null && Mm2023Client.LAST_DAMAGED_TIME >= now - 5000){
            assert client.world != null;
            other = client.world.getEntityById(Mm2023Client.LAST_DAMAGED_BY);
        }
        if(Mm2023Client.LAST_ATTACKED_BY != null && Mm2023Client.LAST_ATTACKED_TIME >= now - 5000 && (Mm2023Client.LAST_DAMAGED_TIME == null || Mm2023Client.LAST_ATTACKED_TIME > Mm2023Client.LAST_DAMAGED_TIME)){
            assert client.world != null;
            other = client.world.getEntityById(Mm2023Client.LAST_ATTACKED_BY);
        }

        if(other instanceof LivingEntity livingEntity){
            otherHealth = livingEntity.getHealth();
            otherAbsorption = livingEntity.getAbsorptionAmount();
            otherMaxHealth = (float)livingEntity.getAttributeValue(EntityAttributes.GENERIC_MAX_HEALTH);

            if(otherHealth > 0 && otherHealth < 0.5f){
                otherHealth = 0.5f;
            }
        }

        float health = client.player.getHealth();
        float absorption = client.player.getAbsorptionAmount();
        float maxHealth = (float)client.player.getAttributeValue(EntityAttributes.GENERIC_MAX_HEALTH);

        if(health > 0 && health < 0.5f){
            health = 0.5f;
        }

        int segmentWidth = 30;
        int segmentCropWidth = 13;
        int segmentHeight = 11;
        int totalSegmentsToShow = (int)Math.ceil(maxHealth / 2);
        int totalAbsorptionSegmentsToShow = (int)Math.ceil(absorption / 2);

        int barX = (context.getScaledWindowWidth() / 2) - 155;
        int barY = context.getScaledWindowHeight() - 37;

        for(int i = 0; i < totalSegmentsToShow; i++) {
            Identifier tex = getHealthSegmentTexture(i, health, false, false);

            context.drawTexture(
                    tex,
                    barX + (i % 10) * segmentCropWidth + (i / 10) * 8,
                    barY - (i / 10) * segmentHeight + 1,
                    0,
                    0,
                    segmentWidth,
                    segmentHeight,
                    segmentWidth,
                    segmentHeight
            );
        }
        for(int i = 0; i < totalAbsorptionSegmentsToShow; i++) {
            Identifier tex = getHealthSegmentTexture(i, absorption, false, true);

            context.drawTexture(
                    tex,
                    barX + (i % 10) * segmentCropWidth + (i / 10) * 8,
                    barY - (i / 10) * segmentHeight + 1,
                    0,
                    0,
                    segmentWidth,
                    segmentHeight,
                    segmentWidth,
                    segmentHeight
            );
        }

        int barOtherX = (context.getScaledWindowWidth() / 2) + 155;
        int totalOtherSegmentsToShow = (int)Math.ceil(maxHealth / 2);
        int totalOtherAbsorptionSegmentsToShow = (int)Math.ceil(otherAbsorption / 2);

        if(otherMaxHealth > 0) {
            for (int i = 0; i < totalOtherSegmentsToShow; i++) {
                Identifier tex = getHealthSegmentTexture(i, otherHealth, true, false);

                context.drawTexture(
                        tex,
                        barOtherX - segmentWidth - (i % 10) * segmentCropWidth - (i / 10) * 8,
                        barY - (i / 10) * segmentHeight + 1,
                        0,
                        0,
                        segmentWidth,
                        segmentHeight,
                        segmentWidth,
                        segmentHeight
                );
            }
            for(int i = 0; i < totalOtherAbsorptionSegmentsToShow; i++) {
                Identifier tex = getHealthSegmentTexture(i, otherAbsorption, true, true);

                context.drawTexture(
                        tex,
                        barOtherX - segmentWidth - (i % 10) * segmentCropWidth - (i / 10) * 8,
                        barY - (i / 10) * segmentHeight + 1,
                        0, 0,
                        segmentWidth,
                        segmentHeight,
                        segmentWidth,
                        segmentHeight
                );
            }
        }

        context.drawText(
                client.textRenderer,
                client.player.getDisplayName(),
                barX + ((int)Math.floor((float)totalSegmentsToShow / 10) * segmentCropWidth),
                barY - ((int)Math.ceil((float)totalSegmentsToShow / 10) * segmentHeight) + 3,
                0xFFFFFF,
                false
        );

        if(otherMaxHealth > 0) {
            context.drawText(
                    client.textRenderer,
                    other.getDisplayName(),
                    barOtherX - ((int) Math.floor((float) totalOtherSegmentsToShow / 10) * segmentCropWidth) - client.textRenderer.getWidth(other.getDisplayName()),
                    barY - ((int) Math.ceil((float) totalOtherSegmentsToShow / 10) * segmentHeight) + 3,
                    0xFFFFFF,
                    false
            );
        }

        ci.cancel();
    }

    @NotNull
    private static Identifier getHealthSegmentTexture(int i, float health, boolean right, boolean absorption) {
        float segmentQuarterHp = i * 2 + 0.5f;
        int segmentHalfHp = i * 2 + 1;
        float segmentThreeQuartersHp = i * 2 + 1.5f;
        int segmentFullHp = i * 2 + 2;

        if(absorption){
            if(right){
                return getHealthSegmentTexture2(health, segmentQuarterHp, segmentHalfHp, segmentThreeQuartersHp, segmentFullHp, ABSORPTION_BAR_FULL_RIGHT, ABSORPTION_BAR_THREE_QUARTERS_RIGHT, ABSORPTION_BAR_HALF_RIGHT, ABSORPTION_BAR_QUARTER_RIGHT, ABSORPTION_BAR_EMPTY_RIGHT);
            }

            return getHealthSegmentTexture2(health, segmentQuarterHp, segmentHalfHp, segmentThreeQuartersHp, segmentFullHp, ABSORPTION_BAR_FULL_LEFT, ABSORPTION_BAR_THREE_QUARTERS_LEFT, ABSORPTION_BAR_HALF_LEFT, ABSORPTION_BAR_QUARTER_LEFT, ABSORPTION_BAR_EMPTY_LEFT);
        }

        if(right){
            return getHealthSegmentTexture2(health, segmentQuarterHp, segmentHalfHp, segmentThreeQuartersHp, segmentFullHp, HEALTH_BAR_FULL_RIGHT, HEALTH_BAR_THREE_QUARTERS_RIGHT, HEALTH_BAR_HALF_RIGHT, HEALTH_BAR_QUARTER_RIGHT, HEALTH_BAR_EMPTY_RIGHT);
        }

        return getHealthSegmentTexture2(health, segmentQuarterHp, segmentHalfHp, segmentThreeQuartersHp, segmentFullHp, HEALTH_BAR_FULL_LEFT, HEALTH_BAR_THREE_QUARTERS_LEFT, HEALTH_BAR_HALF_LEFT, HEALTH_BAR_QUARTER_LEFT, HEALTH_BAR_EMPTY_LEFT);
    }

    private static @NotNull Identifier getHealthSegmentTexture2(float health, float segmentQuarterHp, int segmentHalfHp, float segmentThreeQuartersHp, int segmentFullHp, Identifier healthBarFullLeft, Identifier healthBarThreeQuartersLeft, Identifier healthBarHalfLeft, Identifier healthBarQuarterLeft, Identifier healthBarEmptyLeft) {
        if(health >= segmentFullHp){
            return healthBarFullLeft;
        }
        else if(health >= segmentThreeQuartersHp){
            return healthBarThreeQuartersLeft;
        }
        else if(health >= segmentHalfHp){
            return healthBarHalfLeft;
        }
        else if(health >= segmentQuarterHp){
            return healthBarQuarterLeft;
        }
        return healthBarEmptyLeft;
    }

    public static void injectRenderHotbar(float tickDelta, DrawContext context, CallbackInfo ci){
        MinecraftClient client = MinecraftClient.getInstance();
        assert client.player != null;

        // Slot backgrounds
        int slotWidth = 40;
        int slotHeight = 21;
        int slotBackgroundOffsetY = -2;

        context.drawTexture(HOTBAR_SLOT_LEFT, context.getScaledWindowWidth() / 2 - slotWidth / 2 - ((slotWidth - 10) * 4), context.getScaledWindowHeight() - slotHeight + slotBackgroundOffsetY, 0, 0, slotWidth, slotHeight, slotWidth, slotHeight);
        context.drawTexture(HOTBAR_SLOT_LEFT, context.getScaledWindowWidth() / 2 - slotWidth / 2 - ((slotWidth - 10) * 3), context.getScaledWindowHeight() - slotHeight + slotBackgroundOffsetY, 0, 0, slotWidth, slotHeight, slotWidth, slotHeight);
        context.drawTexture(HOTBAR_SLOT_LEFT, context.getScaledWindowWidth() / 2 - slotWidth / 2 - ((slotWidth - 10) * 2), context.getScaledWindowHeight() - slotHeight + slotBackgroundOffsetY, 0, 0, slotWidth, slotHeight, slotWidth, slotHeight);
        context.drawTexture(HOTBAR_SLOT_LEFT, context.getScaledWindowWidth() / 2 - slotWidth / 2 - (slotWidth - 10), context.getScaledWindowHeight() - slotHeight + slotBackgroundOffsetY, 0, 0, slotWidth, slotHeight, slotWidth, slotHeight);
        context.drawTexture(HOTBAR_SLOT_MIDDLE, context.getScaledWindowWidth() / 2 - slotWidth / 2, context.getScaledWindowHeight() - slotHeight + slotBackgroundOffsetY, 0, 0, slotWidth, slotHeight, slotWidth, slotHeight);
        context.drawTexture(HOTBAR_SLOT_RIGHT, context.getScaledWindowWidth() / 2 - slotWidth / 2 + (slotWidth - 10), context.getScaledWindowHeight() - slotHeight + slotBackgroundOffsetY, 0, 0, slotWidth, slotHeight, slotWidth, slotHeight);
        context.drawTexture(HOTBAR_SLOT_RIGHT, context.getScaledWindowWidth() / 2 - slotWidth / 2 + ((slotWidth - 10) * 2), context.getScaledWindowHeight() - slotHeight + slotBackgroundOffsetY, 0, 0, slotWidth, slotHeight, slotWidth, slotHeight);
        context.drawTexture(HOTBAR_SLOT_RIGHT, context.getScaledWindowWidth() / 2 - slotWidth / 2 + ((slotWidth - 10) * 3), context.getScaledWindowHeight() - slotHeight + slotBackgroundOffsetY, 0, 0, slotWidth, slotHeight, slotWidth, slotHeight);
        context.drawTexture(HOTBAR_SLOT_RIGHT, context.getScaledWindowWidth() / 2 - slotWidth / 2 + ((slotWidth - 10) * 4), context.getScaledWindowHeight() - slotHeight + slotBackgroundOffsetY, 0, 0, slotWidth, slotHeight, slotWidth, slotHeight);

        Arm mainArm = client.player.getMainArm();

        if(mainArm == Arm.LEFT){
            drawOuterSlotBgs(context, slotWidth, slotHeight, HOTBAR_SLOT_LEFT, HOTBAR_OFFHAND_SLOT_RIGHT, slotBackgroundOffsetY);
        }
        else{
            drawOuterSlotBgs(context, slotWidth, slotHeight, HOTBAR_OFFHAND_SLOT_LEFT, HOTBAR_SLOT_RIGHT, slotBackgroundOffsetY);
        }

        // Render items
        int itemRenderOffsetY = 0;
        if(mainArm == Arm.LEFT){
            renderHotbarItem(context, client.player.getInventory().main.get(0), context.getScaledWindowWidth() / 2 - 9 - ((slotWidth - 10) * 5), context.getScaledWindowHeight() - slotHeight + itemRenderOffsetY, tickDelta, client.player, 0);
            renderHotbarItem(context, client.player.getInventory().main.get(1), context.getScaledWindowWidth() / 2 - 9 - ((slotWidth - 10) * 4), context.getScaledWindowHeight() - slotHeight + itemRenderOffsetY, tickDelta, client.player, 1);
            renderHotbarItem(context, client.player.getInventory().main.get(2), context.getScaledWindowWidth() / 2 - 9 - ((slotWidth - 10) * 3), context.getScaledWindowHeight() - slotHeight + itemRenderOffsetY, tickDelta, client.player, 2);
            renderHotbarItem(context, client.player.getInventory().main.get(3), context.getScaledWindowWidth() / 2 - 9 - ((slotWidth - 10) * 2), context.getScaledWindowHeight() - slotHeight + itemRenderOffsetY, tickDelta, client.player, 3);
            renderHotbarItem(context, client.player.getInventory().main.get(4), context.getScaledWindowWidth() / 2 - 9 - (slotWidth - 10), context.getScaledWindowHeight() - slotHeight + itemRenderOffsetY, tickDelta, client.player, 4);
            renderHotbarItem(context, client.player.getInventory().main.get(5), context.getScaledWindowWidth() / 2 - 9 + (slotWidth - 10), context.getScaledWindowHeight() - slotHeight + itemRenderOffsetY, tickDelta, client.player, 5);
            renderHotbarItem(context, client.player.getInventory().main.get(6), context.getScaledWindowWidth() / 2 - 9 + ((slotWidth - 10) * 2), context.getScaledWindowHeight() - slotHeight + itemRenderOffsetY, tickDelta, client.player, 6);
            renderHotbarItem(context, client.player.getInventory().main.get(7), context.getScaledWindowWidth() / 2 - 9 + ((slotWidth - 10) * 3), context.getScaledWindowHeight() - slotHeight + itemRenderOffsetY, tickDelta, client.player, 7);
            renderHotbarItem(context, client.player.getInventory().main.get(8), context.getScaledWindowWidth() / 2 - 9 + ((slotWidth - 10) * 4), context.getScaledWindowHeight() - slotHeight + itemRenderOffsetY, tickDelta, client.player, 8);
            renderHotbarItem(context, client.player.getOffHandStack(), context.getScaledWindowWidth() / 2 - 9 + ((slotWidth - 10) * 5), context.getScaledWindowHeight() - slotHeight + itemRenderOffsetY, tickDelta, client.player, 9);
        }
        else{
            renderHotbarItem(context, client.player.getOffHandStack(), context.getScaledWindowWidth() / 2 - 9 - ((slotWidth - 10) * 5), context.getScaledWindowHeight() - slotHeight + itemRenderOffsetY, tickDelta, client.player, 0);
            renderHotbarItem(context, client.player.getInventory().main.get(0), context.getScaledWindowWidth() / 2 - 9 - ((slotWidth - 10) * 4), context.getScaledWindowHeight() - slotHeight + itemRenderOffsetY, tickDelta, client.player, 1);
            renderHotbarItem(context, client.player.getInventory().main.get(1), context.getScaledWindowWidth() / 2 - 9 - ((slotWidth - 10) * 3), context.getScaledWindowHeight() - slotHeight + itemRenderOffsetY, tickDelta, client.player, 2);
            renderHotbarItem(context, client.player.getInventory().main.get(2), context.getScaledWindowWidth() / 2 - 9 - ((slotWidth - 10) * 2), context.getScaledWindowHeight() - slotHeight + itemRenderOffsetY, tickDelta, client.player, 3);
            renderHotbarItem(context, client.player.getInventory().main.get(3), context.getScaledWindowWidth() / 2 - 9 - (slotWidth - 10), context.getScaledWindowHeight() - slotHeight + itemRenderOffsetY, tickDelta, client.player, 4);
            renderHotbarItem(context, client.player.getInventory().main.get(4), context.getScaledWindowWidth() / 2 - 9 + (slotWidth - 10), context.getScaledWindowHeight() - slotHeight + itemRenderOffsetY, tickDelta, client.player, 5);
            renderHotbarItem(context, client.player.getInventory().main.get(5), context.getScaledWindowWidth() / 2 - 9 + ((slotWidth - 10) * 2), context.getScaledWindowHeight() - slotHeight + itemRenderOffsetY, tickDelta, client.player, 6);
            renderHotbarItem(context, client.player.getInventory().main.get(6), context.getScaledWindowWidth() / 2 - 9 + ((slotWidth - 10) * 3), context.getScaledWindowHeight() - slotHeight + itemRenderOffsetY, tickDelta, client.player, 7);
            renderHotbarItem(context, client.player.getInventory().main.get(7), context.getScaledWindowWidth() / 2 - 9 + ((slotWidth - 10) * 4), context.getScaledWindowHeight() - slotHeight + itemRenderOffsetY, tickDelta, client.player, 8);
            renderHotbarItem(context, client.player.getInventory().main.get(8), context.getScaledWindowWidth() / 2 - 9 + ((slotWidth - 10) * 5), context.getScaledWindowHeight() - slotHeight + itemRenderOffsetY, tickDelta, client.player, 9);
        }

        // Render selected slot overlay
        if(mainArm == Arm.LEFT){
            switch(client.player.getInventory().selectedSlot) {
                case 0:
                    context.drawTexture(HOTBAR_SELECTED_SLOT_LEFT, context.getScaledWindowWidth() / 2 - slotWidth / 2 - ((slotWidth - 10) * 5), context.getScaledWindowHeight() - slotHeight + slotBackgroundOffsetY, 0, 0, slotWidth, slotHeight, slotWidth, slotHeight);
                    break;
                case 1:
                    context.drawTexture(HOTBAR_SELECTED_SLOT_LEFT, context.getScaledWindowWidth() / 2 - slotWidth / 2 - ((slotWidth - 10) * 4), context.getScaledWindowHeight() - slotHeight + slotBackgroundOffsetY, 0, 0, slotWidth, slotHeight, slotWidth, slotHeight);
                    break;
                case 2:
                    context.drawTexture(HOTBAR_SELECTED_SLOT_LEFT, context.getScaledWindowWidth() / 2 - slotWidth / 2 - ((slotWidth - 10) * 3), context.getScaledWindowHeight() - slotHeight + slotBackgroundOffsetY, 0, 0, slotWidth, slotHeight, slotWidth, slotHeight);
                    break;
                case 3:
                    context.drawTexture(HOTBAR_SELECTED_SLOT_LEFT, context.getScaledWindowWidth() / 2 - slotWidth / 2 - ((slotWidth - 10) * 2), context.getScaledWindowHeight() - slotHeight + slotBackgroundOffsetY, 0, 0, slotWidth, slotHeight, slotWidth, slotHeight);
                    break;
                case 4:
                    context.drawTexture(HOTBAR_SELECTED_SLOT_LEFT, context.getScaledWindowWidth() / 2 - slotWidth / 2 - (slotWidth - 10), context.getScaledWindowHeight() - slotHeight + slotBackgroundOffsetY, 0, 0, slotWidth, slotHeight, slotWidth, slotHeight);
                    break;
                case 5:
                    context.drawTexture(HOTBAR_SELECTED_SLOT_RIGHT, context.getScaledWindowWidth() / 2 - slotWidth / 2 + (slotWidth - 10), context.getScaledWindowHeight() - slotHeight + slotBackgroundOffsetY, 0, 0, slotWidth, slotHeight, slotWidth, slotHeight);
                    break;
                case 6:
                    context.drawTexture(HOTBAR_SELECTED_SLOT_RIGHT, context.getScaledWindowWidth() / 2 - slotWidth / 2 + ((slotWidth - 10) * 2), context.getScaledWindowHeight() - slotHeight + slotBackgroundOffsetY, 0, 0, slotWidth, slotHeight, slotWidth, slotHeight);
                    break;
                case 7:
                    context.drawTexture(HOTBAR_SELECTED_SLOT_RIGHT, context.getScaledWindowWidth() / 2 - slotWidth / 2 + ((slotWidth - 10) * 3), context.getScaledWindowHeight() - slotHeight + slotBackgroundOffsetY, 0, 0, slotWidth, slotHeight, slotWidth, slotHeight);
                    break;
                case 8:
                    context.drawTexture(HOTBAR_SELECTED_SLOT_RIGHT, context.getScaledWindowWidth() / 2 - slotWidth / 2 + ((slotWidth - 10) * 4), context.getScaledWindowHeight() - slotHeight + slotBackgroundOffsetY, 0, 0, slotWidth, slotHeight, slotWidth, slotHeight);
                    break;
            }
        }
        else{
            switch(client.player.getInventory().selectedSlot) {
                case 0:
                    context.drawTexture(HOTBAR_SELECTED_SLOT_LEFT, context.getScaledWindowWidth() / 2 - slotWidth / 2 - ((slotWidth - 10) * 4), context.getScaledWindowHeight() - slotHeight + slotBackgroundOffsetY, 0, 0, slotWidth, slotHeight, slotWidth, slotHeight);
                    break;
                case 1:
                    context.drawTexture(HOTBAR_SELECTED_SLOT_LEFT, context.getScaledWindowWidth() / 2 - slotWidth / 2 - ((slotWidth - 10) * 3), context.getScaledWindowHeight() - slotHeight + slotBackgroundOffsetY, 0, 0, slotWidth, slotHeight, slotWidth, slotHeight);
                    break;
                case 2:
                    context.drawTexture(HOTBAR_SELECTED_SLOT_LEFT, context.getScaledWindowWidth() / 2 - slotWidth / 2 - ((slotWidth - 10) * 2), context.getScaledWindowHeight() - slotHeight + slotBackgroundOffsetY, 0, 0, slotWidth, slotHeight, slotWidth, slotHeight);
                    break;
                case 3:
                    context.drawTexture(HOTBAR_SELECTED_SLOT_LEFT, context.getScaledWindowWidth() / 2 - slotWidth / 2 - (slotWidth - 10), context.getScaledWindowHeight() - slotHeight + slotBackgroundOffsetY, 0, 0, slotWidth, slotHeight, slotWidth, slotHeight);
                    break;
                case 4:
                    context.drawTexture(HOTBAR_SELECTED_SLOT_RIGHT, context.getScaledWindowWidth() / 2 - slotWidth / 2 + (slotWidth - 10), context.getScaledWindowHeight() - slotHeight + slotBackgroundOffsetY, 0, 0, slotWidth, slotHeight, slotWidth, slotHeight);
                    break;
                case 5:
                    context.drawTexture(HOTBAR_SELECTED_SLOT_RIGHT, context.getScaledWindowWidth() / 2 - slotWidth / 2 + ((slotWidth - 10) * 2), context.getScaledWindowHeight() - slotHeight + slotBackgroundOffsetY, 0, 0, slotWidth, slotHeight, slotWidth, slotHeight);
                    break;
                case 6:
                    context.drawTexture(HOTBAR_SELECTED_SLOT_RIGHT, context.getScaledWindowWidth() / 2 - slotWidth / 2 + ((slotWidth - 10) * 3), context.getScaledWindowHeight() - slotHeight + slotBackgroundOffsetY, 0, 0, slotWidth, slotHeight, slotWidth, slotHeight);
                    break;
                case 7:
                    context.drawTexture(HOTBAR_SELECTED_SLOT_RIGHT, context.getScaledWindowWidth() / 2 - slotWidth / 2 + ((slotWidth - 10) * 4), context.getScaledWindowHeight() - slotHeight + slotBackgroundOffsetY, 0, 0, slotWidth, slotHeight, slotWidth, slotHeight);
                    break;
                case 8:
                    context.drawTexture(HOTBAR_SELECTED_SLOT_RIGHT, context.getScaledWindowWidth() / 2 - slotWidth / 2 + ((slotWidth - 10) * 5), context.getScaledWindowHeight() - slotHeight + slotBackgroundOffsetY, 0, 0, slotWidth, slotHeight, slotWidth, slotHeight);
                    break;
            }
        }

        // Player preview
        int playerPreviewX = context.getScaledWindowWidth() / 2;
        int playerPreviewY = context.getScaledWindowHeight() - 5;
        InventoryScreen.drawEntity(
                context,
                playerPreviewX,
                playerPreviewY,
                8,
                0.5f,
                0.5f,
                client.player
        );
    }

    private static void drawOuterSlotBgs(DrawContext context, int slotWidth, int slotHeight, Identifier leftTexture, Identifier rightTexture, int slotBackgroundOffsetY) {
        context.drawTexture(leftTexture, context.getScaledWindowWidth() / 2 - slotWidth / 2 - ((slotWidth - 10) * 5), context.getScaledWindowHeight() - slotHeight + slotBackgroundOffsetY, 0, 0, slotWidth, slotHeight, slotWidth, slotHeight);
        context.drawTexture(rightTexture, context.getScaledWindowWidth() / 2 - slotWidth / 2 + ((slotWidth - 10) * 5), context.getScaledWindowHeight() - slotHeight + slotBackgroundOffsetY, 0, 0, slotWidth, slotHeight, slotWidth, slotHeight);
    }

    private static void renderHotbarItem(DrawContext context, ItemStack stack, int x, int y, float tickDelta, PlayerEntity player, int seed) {
        if (stack.isEmpty()) {
            return;
        }
        float f = (float)stack.getBobbingAnimationTime() - tickDelta;
        if (f > 0.0f) {
            float g = 1.0f + f / 5.0f;
            context.getMatrices().push();
            context.getMatrices().translate(x + 8, y + 12, 0.0f);
            context.getMatrices().scale(1.0f / g, (g + 1.0f) / 2.0f, 1.0f);
            context.getMatrices().scale(0.95f, 0.95f, 0.95f);
            context.getMatrices().translate(-(x + 8), -(y + 12), 0.0f);
        }
        context.drawItem(player, stack, x, y, seed);
        if (f > 0.0f) {
            context.getMatrices().pop();
        }
        context.drawItemInSlot(MinecraftClient.getInstance().textRenderer, stack, x, y);
    }

    public static void injectRenderStatusEffectOverlay(DrawContext context, CallbackInfo ci, InGameHud thisObject) {
        MinecraftClient minecraftClient = MinecraftClient.getInstance();
        assert minecraftClient.player != null;
        assert minecraftClient.world != null;
        Collection<StatusEffectInstance> collection = minecraftClient.player.getStatusEffects();
        RenderSystem.enableBlend();
        StatusEffectSpriteManager statusEffectSpriteManager = minecraftClient.getStatusEffectSpriteManager();
        int labelWidth = 60;
        int labelHeight = 20;
        int x = context.getScaledWindowWidth() - 11;
        int y = 6;
        int xStep = labelWidth - 7;
        {// Draw backgrounds
            int backgroundX = x;
            if(collection.stream().anyMatch((effectInstance) -> !effectInstance.isAmbient())) {
                context.drawTexture(EFFECT_BACKGROUND_TEXTURE, backgroundX, y, 0, 0, labelWidth, labelHeight, labelWidth, labelHeight);
            }
            for (StatusEffectInstance statusEffectInstance : Ordering.natural().reverse().sortedCopy(collection)) {
                if(statusEffectInstance.isAmbient() || !statusEffectInstance.shouldShowIcon()){
                    continue;
                }
                backgroundX -= xStep;
                context.drawTexture(EFFECT_BACKGROUND_TEXTURE, backgroundX, y, 0, 0, labelWidth, labelHeight, labelWidth, labelHeight);
            }
        }
        for (StatusEffectInstance statusEffectInstance : Ordering.natural().reverse().sortedCopy(collection)) {
            if(statusEffectInstance.isAmbient() || !statusEffectInstance.shouldShowIcon()){
                continue;
            }
            StatusEffect statusEffect = statusEffectInstance.getEffectType();

            x -= xStep;
            float alpha = 1.0f;

            if (statusEffectInstance.isDurationBelow(200)) {
                int m = statusEffectInstance.getDuration();
                alpha = MathHelper.clamp((float)m / 10.0f / 5.0f * 0.5f, 0.0f, 0.5f) + MathHelper.cos((float)m * (float)Math.PI / 5.0f) * MathHelper.clamp((float)x / 10.0f * 0.25f, 0.0f, 0.25f);
            }

            Sprite sprite = statusEffectSpriteManager.getSprite(statusEffect);
            context.setShaderColor(1.0f, 1.0f, 1.0f, alpha);
            context.drawSprite(x + 9, y + 4, 0, 12, 12, sprite);
            context.setShaderColor(1.0f, 1.0f, 1.0f, 1.0f);
            Text durationText = StatusEffectUtil.getDurationText(statusEffectInstance, 1.0f);
            context.drawText(
                    thisObject.getTextRenderer(),
                    durationText,
                    x + 14 + 10,
                    y + 7,
                    statusEffectInstance.isDurationBelow(200) ? 0xFF5555 : 0xFFFFFF,
                    false
            );
        }
    }
}
