package uk.co.cablepost.mm2023;

import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.entity.event.v1.ServerLivingEntityEvents;
import net.fabricmc.fabric.api.event.lifecycle.v1.ServerTickEvents;
import net.fabricmc.fabric.api.networking.v1.ServerPlayConnectionEvents;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.util.Identifier;
import net.minecraft.world.GameMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.co.cablepost.mm2023.init.MinigameUtilBlockInit;
import uk.co.cablepost.mm2023.init.SwordInit;
import uk.co.cablepost.mm2023.server.ConnectingPlayerModel;
import uk.co.cablepost.mm2023.server.MmStaticServerData;
import uk.co.cablepost.mm2023.server.minigame.StaticMinigameUtils;

import java.util.*;

public class Mm2023 implements ModInitializer {
    public static final String MOD_ID = "mm2023";
    public static final String API_BASE_URL = "https://mm2023api.cablepost.co.uk/";
    public static final Logger LOGGER = LoggerFactory.getLogger(MOD_ID);

    public static final Identifier SET_LAST_DAMAGE_PACKET_ID = new Identifier(MOD_ID, "set_last_damage_packet");
    public static final Identifier SET_LAST_ATTACK_PACKET_ID = new Identifier(MOD_ID, "set_last_attack_packet");
    public static final Identifier HIDE_AUX_GAME_PACKET_ID = new Identifier(MOD_ID, "hide_aux_game_packet");
    public static final Identifier MINIGAME_OVER_PACKET_ID = new Identifier(MOD_ID, "minigame_over_packet");
    public static final Identifier SET_INPUTS_ALLOWED_PACKET_ID = new Identifier(MOD_ID, "set_inputs_allowed_packet");

    @Override
    public void onInitialize() {
        new SwordInit().commonInit();
        new MinigameUtilBlockInit().commonInit();

        ServerPlayConnectionEvents.JOIN.register((handler, sender, server) -> {
            String playerUuid = handler.player.getGameProfile().getId().toString();
            if(
                (System.getenv("MM_DEBUG_MINIGAME") != null && System.getenv("MM_DEBUG_MINIGAME").trim().equalsIgnoreCase("true")) ||
                System.getenv("MM_API_TOKEN") != null
            ) {
                handler.player.interactionManager.changeGameMode(GameMode.SPECTATOR);
                StaticMinigameUtils.setInputsAllowed(handler.player, false, false);
            }
            else{
                StaticMinigameUtils.setInputsAllowed(handler.player, true, true);
            }

            if(!MmStaticServerData.CONNECTED_PLAYERS.contains(playerUuid)) {
                MmStaticServerData.CONNECTED_PLAYERS.add(playerUuid);

                if(System.getenv("MM_DEBUG_MINIGAME") != null && System.getenv("MM_DEBUG_MINIGAME").trim().equalsIgnoreCase("true")) {
                    MmStaticServerData.CONNECTING_PLAYERS.put(playerUuid, new ConnectingPlayerModel(server.getCurrentPlayerCount() % 2));
                }
            }
        });

        ServerPlayConnectionEvents.DISCONNECT.register((handler, server) -> {
            MmStaticServerData.CONNECTED_PLAYERS.remove(handler.player.getGameProfile().getId().toString());
        });

        ServerTickEvents.START_WORLD_TICK.register((world) -> {
            if(MmStaticServerData.MINIGAME_INSTANCE != null){
                MmStaticServerData.MINIGAME_INSTANCE.onTick(world);
            }
        });

        ServerLivingEntityEvents.ALLOW_DEATH.register((entity, damageSource, damageAmount) -> {
            if(!(entity instanceof PlayerEntity)){
                return true;
            }

            ServerPlayerEntity serverPlayerEntity = Objects.requireNonNull(Objects.requireNonNull(entity.getWorld().getServer()).getPlayerManager().getPlayer(entity.getUuid()));

            StaticMinigameUtils.setInputsAllowed(serverPlayerEntity, !MmStaticServerData.DISABLE_MOVE_ON_DEATH, !MmStaticServerData.DISABLE_LOOK_ON_DEATH);

            if(!MmStaticServerData.SPECTATOR_ON_DEATH){
                return true;
            }

            entity.setHealth(20);
            entity.setOnFire(false);
            entity.clearStatusEffects();

            serverPlayerEntity.changeGameMode(GameMode.SPECTATOR);
            serverPlayerEntity.setVelocity(0f, 2f, 0f);
            serverPlayerEntity.setPitch(90f);

            return false;
        });
    }
}
