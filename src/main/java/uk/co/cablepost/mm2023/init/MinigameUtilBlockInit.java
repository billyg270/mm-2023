package uk.co.cablepost.mm2023.init;

import net.fabricmc.fabric.api.blockrenderlayer.v1.BlockRenderLayerMap;
import net.fabricmc.fabric.api.item.v1.FabricItemSettings;
import net.fabricmc.fabric.api.itemgroup.v1.FabricItemGroup;
import net.fabricmc.fabric.api.networking.v1.ServerPlayNetworking;
import net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings;
import net.fabricmc.fabric.api.object.builder.v1.block.entity.FabricBlockEntityTypeBuilder;
import net.fabricmc.fabric.api.screenhandler.v1.ExtendedScreenHandlerType;
import net.minecraft.block.Blocks;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.client.gui.screen.ingame.HandledScreens;
import net.minecraft.client.render.RenderLayer;
import net.minecraft.item.BlockItem;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.registry.Registries;
import net.minecraft.registry.Registry;
import net.minecraft.screen.ScreenHandlerType;
import net.minecraft.text.Text;
import net.minecraft.util.Identifier;
import uk.co.cablepost.mm2023.Mm2023;
import uk.co.cablepost.mm2023.block.minigameUtil.markerBlock.MarkerBlock;
import uk.co.cablepost.mm2023.block.minigameUtil.markerBlock.MarkerBlockEntity;
import uk.co.cablepost.mm2023.block.minigameUtil.markerBlock.MarkerBlockScreen;
import uk.co.cablepost.mm2023.block.minigameUtil.markerBlock.MarkerBlockScreenHandler;
import uk.co.cablepost.mm2023.block.minigameUtil.shopBlock.ShopBlock;
import uk.co.cablepost.mm2023.block.minigameUtil.shopBlock.ShopBlockEntity;
import uk.co.cablepost.mm2023.block.minigameUtil.shopBlock.ShopBlockScreen;
import uk.co.cablepost.mm2023.block.minigameUtil.shopBlock.ShopBlockScreenHandler;
import uk.co.cablepost.mm2023.block.minigameUtil.teamBarrierBlock.TeamBarrierBlock;
import uk.co.cablepost.mm2023.block.minigameUtil.teamBarrierBlock.TeamBarrierBlockEntity;

public class MinigameUtilBlockInit implements IInit {
    // Marker
    public static final Identifier MARKER_BLOCK_IDENTIFIER  = new Identifier(Mm2023.MOD_ID, "marker");
    public static final MarkerBlock MARKER_BLOCK  = new MarkerBlock(FabricBlockSettings.create().strength(-1.0f, 3600000.0f));
    public static final BlockEntityType<MarkerBlockEntity> MARKER_BLOCK_ENTITY = Registry.register(
            Registries.BLOCK_ENTITY_TYPE,
            MARKER_BLOCK_IDENTIFIER,
            FabricBlockEntityTypeBuilder.create(MarkerBlockEntity::new, MARKER_BLOCK).build()
    );
    public static final BlockItem MARKER_BLOCK_ITEM  = new BlockItem(MARKER_BLOCK, new FabricItemSettings());
    public static final ScreenHandlerType<MarkerBlockScreenHandler> MARKER_BLOCK_SCREEN_HANDLER = new ExtendedScreenHandlerType<>(MarkerBlockScreenHandler::new);

    // Team barrier
    public static final Identifier TEAM_BARRIER_BLOCK_IDENTIFIER  = new Identifier(Mm2023.MOD_ID, "team_barrier");
    public static final TeamBarrierBlock TEAM_BARRIER_BLOCK  = new TeamBarrierBlock(FabricBlockSettings.create().strength(-1.0f, 3600000.0f).noCollision().nonOpaque().allowsSpawning(Blocks::never).solidBlock(Blocks::never).suffocates(Blocks::never).blockVision(Blocks::never));
    public static final BlockEntityType<TeamBarrierBlockEntity> TEAM_BARRIER_BLOCK_ENTITY = Registry.register(
            Registries.BLOCK_ENTITY_TYPE,
            TEAM_BARRIER_BLOCK_IDENTIFIER,
            FabricBlockEntityTypeBuilder.create(TeamBarrierBlockEntity::new, TEAM_BARRIER_BLOCK).build()
    );
    public static final BlockItem TEAM_BARRIER_BLOCK_ITEM  = new BlockItem(TEAM_BARRIER_BLOCK, new FabricItemSettings());

    // Shop
    public static final Identifier SHOP_BLOCK_IDENTIFIER  = new Identifier(Mm2023.MOD_ID, "shop");
    public static final ShopBlock SHOP_BLOCK  = new ShopBlock(FabricBlockSettings.create().strength(-1.0f, 3600000.0f));
    public static final BlockEntityType<ShopBlockEntity> SHOP_BLOCK_ENTITY = Registry.register(
            Registries.BLOCK_ENTITY_TYPE,
            SHOP_BLOCK_IDENTIFIER,
            FabricBlockEntityTypeBuilder.create(ShopBlockEntity::new, SHOP_BLOCK).build()
    );
    public static final BlockItem SHOP_BLOCK_ITEM  = new BlockItem(SHOP_BLOCK, new FabricItemSettings());
    public static final ScreenHandlerType<ShopBlockScreenHandler> SHOP_BLOCK_SCREEN_HANDLER = new ExtendedScreenHandlerType<>(ShopBlockScreenHandler::new);


    // Item group
    private static final ItemGroup ITEM_GROUP = FabricItemGroup.builder()
        .icon(() -> new ItemStack(MARKER_BLOCK_ITEM))
        .displayName(Text.translatable("itemGroup.mm2023.minigame_util_block"))
        .entries((context, entries) -> {
            entries.add(MARKER_BLOCK);
            entries.add(TEAM_BARRIER_BLOCK);
            entries.add(SHOP_BLOCK);
        })
        .build()
    ;

    @Override
    public void commonInit() {
        // Marker
        Registry.register(Registries.BLOCK, MARKER_BLOCK_IDENTIFIER, MARKER_BLOCK);
        Registry.register(Registries.ITEM, MARKER_BLOCK_IDENTIFIER, MARKER_BLOCK_ITEM);
        Registry.register(Registries.SCREEN_HANDLER, MARKER_BLOCK_IDENTIFIER, MARKER_BLOCK_SCREEN_HANDLER);

        ServerPlayNetworking.registerGlobalReceiver(new Identifier(Mm2023.MOD_ID, "update_marker"), (server, player, handler, buf, responseSender) -> {
            String newTag = buf.readString();

            server.executeSync(() -> {if (player.isCreative() && player.currentScreenHandler instanceof MarkerBlockScreenHandler screenHandler) {
                    screenHandler.setTag(newTag);
                }
            });
        });

        // Team barrier
        Registry.register(Registries.BLOCK, TEAM_BARRIER_BLOCK_IDENTIFIER, TEAM_BARRIER_BLOCK);
        Registry.register(Registries.ITEM, TEAM_BARRIER_BLOCK_IDENTIFIER, TEAM_BARRIER_BLOCK_ITEM);

        // Shop
        Registry.register(Registries.BLOCK, SHOP_BLOCK_IDENTIFIER, SHOP_BLOCK);
        Registry.register(Registries.ITEM, SHOP_BLOCK_IDENTIFIER, SHOP_BLOCK_ITEM);
        Registry.register(Registries.SCREEN_HANDLER, SHOP_BLOCK_IDENTIFIER, SHOP_BLOCK_SCREEN_HANDLER);

        // Item group
        Registry.register(Registries.ITEM_GROUP, new Identifier(Mm2023.MOD_ID, "minigame_util_blocks"), ITEM_GROUP);
    }

    @Override
    public void clientInit() {
        // Marker
        HandledScreens.register(MinigameUtilBlockInit.MARKER_BLOCK_SCREEN_HANDLER, MarkerBlockScreen::new);

        // Team barrier
        BlockRenderLayerMap.INSTANCE.putBlock(TEAM_BARRIER_BLOCK, RenderLayer.getTranslucent());

        // Shop
        HandledScreens.register(MinigameUtilBlockInit.SHOP_BLOCK_SCREEN_HANDLER, ShopBlockScreen::new);
    }
}
