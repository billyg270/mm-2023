package uk.co.cablepost.mm2023.init;

public interface IInit {
    public void commonInit();
    public void clientInit();
}
