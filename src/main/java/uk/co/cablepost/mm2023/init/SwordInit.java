package uk.co.cablepost.mm2023.init;

import net.fabricmc.fabric.api.item.v1.FabricItemSettings;
import net.fabricmc.fabric.api.itemgroup.v1.ItemGroupEvents;
import net.minecraft.client.item.ModelPredicateProviderRegistry;
import net.minecraft.item.ItemGroups;
import net.minecraft.item.Items;
import net.minecraft.item.ToolMaterials;
import net.minecraft.registry.Registries;
import net.minecraft.registry.Registry;
import net.minecraft.util.Identifier;
import uk.co.cablepost.mm2023.Mm2023;
import uk.co.cablepost.mm2023.item.SwordItem;

public class SwordInit implements IInit {
    public static final SwordItem WOOD_SWORD_ITEM = new SwordItem(ToolMaterials.WOOD, 2, 20f, new FabricItemSettings());
    public static final SwordItem STONE_SWORD_ITEM = new SwordItem(ToolMaterials.STONE, 2, 20f, new FabricItemSettings());
    public static final SwordItem IRON_SWORD_ITEM = new SwordItem(ToolMaterials.IRON, 2, 20f, new FabricItemSettings());
    public static final SwordItem GOLD_SWORD_ITEM = new SwordItem(ToolMaterials.GOLD, 2, 20f, new FabricItemSettings());
    public static final SwordItem DIAMOND_SWORD_ITEM = new SwordItem(ToolMaterials.DIAMOND, 2, 20f, new FabricItemSettings());
    public static final SwordItem NETHERITE_SWORD_ITEM = new SwordItem(ToolMaterials.NETHERITE, 2, 20f, new FabricItemSettings());

    @Override
    public void commonInit() {
        Registry.register(Registries.ITEM, new Identifier(Mm2023.MOD_ID, "wood_sword"), WOOD_SWORD_ITEM);

        ItemGroupEvents.modifyEntriesEvent(ItemGroups.COMBAT).register(content -> {
            content.addAfter(Items.WOODEN_SWORD, WOOD_SWORD_ITEM);
        });

        Registry.register(Registries.ITEM, new Identifier(Mm2023.MOD_ID, "stone_sword"), STONE_SWORD_ITEM);

        ItemGroupEvents.modifyEntriesEvent(ItemGroups.COMBAT).register(content -> {
            content.addAfter(Items.STONE_SWORD, STONE_SWORD_ITEM);
        });

        Registry.register(Registries.ITEM, new Identifier(Mm2023.MOD_ID, "iron_sword"), IRON_SWORD_ITEM);

        ItemGroupEvents.modifyEntriesEvent(ItemGroups.COMBAT).register(content -> {
            content.addAfter(Items.IRON_SWORD, IRON_SWORD_ITEM);
        });

        Registry.register(Registries.ITEM, new Identifier(Mm2023.MOD_ID, "gold_sword"), GOLD_SWORD_ITEM);

        ItemGroupEvents.modifyEntriesEvent(ItemGroups.COMBAT).register(content -> {
            content.addAfter(Items.GOLDEN_SWORD, GOLD_SWORD_ITEM);
        });

        Registry.register(Registries.ITEM, new Identifier(Mm2023.MOD_ID, "diamond_sword"), DIAMOND_SWORD_ITEM);

        ItemGroupEvents.modifyEntriesEvent(ItemGroups.COMBAT).register(content -> {
            content.addAfter(Items.DIAMOND_SWORD, DIAMOND_SWORD_ITEM);
        });

        Registry.register(Registries.ITEM, new Identifier(Mm2023.MOD_ID, "netherite_sword"), NETHERITE_SWORD_ITEM);

        ItemGroupEvents.modifyEntriesEvent(ItemGroups.COMBAT).register(content -> {
            content.addAfter(Items.NETHERITE_SWORD, NETHERITE_SWORD_ITEM);
        });
    }

    @Override
    public void clientInit() {
        registerBlocking(WOOD_SWORD_ITEM);
        registerBlocking(STONE_SWORD_ITEM);
        registerBlocking(IRON_SWORD_ITEM);
        registerBlocking(GOLD_SWORD_ITEM);
        registerBlocking(DIAMOND_SWORD_ITEM);
        registerBlocking(NETHERITE_SWORD_ITEM);
    }

    private void registerBlocking(SwordItem swordItem) {
        ModelPredicateProviderRegistry.register(swordItem, new Identifier("blocking"), (stack, world, entity, seed) -> entity != null && entity.isUsingItem() && entity.getActiveItem() == stack ? 1.0f : 0.0f);
    }
}
